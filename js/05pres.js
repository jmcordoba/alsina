function makeRequest(url) {
	var http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	http_request.open('GET', url, false);
	http_request.send(null)
	return http_request;
}

function recargaPresupuesto() {
	//
	anyo    = document.getElementById('any').value;
	piso    = document.getElementById('pisoSelect').value;
	parking = document.getElementById('parkingSelect').value;
	traster = document.getElementById('trasteroSelect').value;
	moto    = document.getElementById('motoParkingSelect').value;
	//
	location.href="menu.php?pagina=presupost&any="+anyo+"&piso="+piso +"&parking="+parking+"&traster="+traster+"&moto="+moto;
}

function calcula(){
	
	var anyo       = document.getElementById('any').value;
	var idPiso     = document.getElementById('pisoSelect').value;			// No,0-25
	var idParking  = document.getElementById('parkingSelect').value;		// No,26-60
	var idTrastero = document.getElementById('trasteroSelect').value;		// Si,No
	var idMotoP    = document.getElementById('motoParkingSelect').value;	// Si,No
	
	if(idPiso=='No' && idParking=='No' && idTrastero=='No' && idMotoP=='No') {
		
		//alert('Informació:\nSel·lecciona algún inmoble per tal de poder calcular les cuotes corresponents.');
		
	} else {

		var valor = makeRequest('mod/calculaAnualidad.php?idPiso='+idPiso+'&idParking='+idParking+'&idTrastero='+idTrastero+'&idMotoP='+idMotoP+'&any='+anyo).responseText;
		
		var valorA = valor/1;
		valorA     = valorA.toFixed(2)+' €';
		valorA     = valorA.replace(".",",");
		
		var valorT = valor/4;
		valorT     = valorT.toFixed(2)+' €';
		valorT     = valorT.replace(".",",");
		
		document.getElementById('cuotaAnual').innerHTML = valorA;
		document.getElementById('cuotaTrim').innerHTML  = valorT;
	}
}

function simula(){
	
	var anyo       = document.getElementById('any').value;
	var idPiso     = document.getElementById('pisoSelect').value;			// No,0-25
	var idParking  = document.getElementById('parkingSelect').value;		// No,26-60
	var idTrastero = document.getElementById('trasteroSelect').value;		// Si,No
	var idMotoP    = document.getElementById('motoParkingSelect').value;	// Si,No

	if(idPiso=='No' && idParking=='No' && idTrastero=='No' && idMotoP=='No') {
		
		//alert('Informació:\nSel·lecciona algún inmoble per tal de poder simular les cuotes corresponents.');
		
	} else {
		
		var inputComun = document.getElementById('inputComun').value;
		var inputEsc85 = document.getElementById('inputEsc85').value;
		var inputEsc91 = document.getElementById('inputEsc91').value;
		var inputEsc95 = document.getElementById('inputEsc95').value;
		var inputEscMV = document.getElementById('inputEscMV').value;
		var inputEscPK = document.getElementById('inputEscPK').value;
		var inputAdmin = document.getElementById('inputAdmin').value;
		var inputAdminCasa = document.getElementById('inputAdminCasa').value;
		var inputAdminPark = document.getElementById('inputAdminPark').value;
		
		var simu = '&inputComun='+inputComun+'&inputEsc85='+inputEsc85+'&inputEsc91='+inputEsc91+'&inputEsc95='+inputEsc95+'&inputEscMV='+inputEscMV+'&inputEscPK='+inputEscPK+'&inputAdmin='+inputAdmin;
		simu    += '&inputAdminCasa='+inputAdminCasa+'&inputAdminPark='+inputAdminPark;
		
		var valor = makeRequest('mod/simulaAnualidad.php?idPiso='+idPiso+'&idParking='+idParking+'&idTrastero='+idTrastero+'&idMotoP='+idMotoP+simu+'&any='+anyo).responseText;
		
		var valorA = valor/1;
		valorA     = valorA.toFixed(2)+' €';
		valorA     = valorA.replace(".",",");
		
		var valorT = valor/4;
		valorT     = valorT.toFixed(2)+' €';
		valorT     = valorT.replace(".",",");
		
		document.getElementById('cuotaAnualSimulada').innerHTML = valorA;
		document.getElementById('cuotaTrimSimulada').innerHTML  = valorT;
	}
}

function recupera() {
	alert('Informació:\nEncara no funciona, pero si clicas F5 tornes a començar igualment.');
}

function calculaTotalSimulacion() {

	var valorComun = document.getElementById('inputComun').value;
	var valorEsc85 = document.getElementById('inputEsc85').value;
	var valorEsc91 = document.getElementById('inputEsc91').value;
	var valorEsc95 = document.getElementById('inputEsc95').value;
	var valorEscMV = document.getElementById('inputEscMV').value;
	var valorEscPK = document.getElementById('inputEscPK').value;
	var valorEscAd = document.getElementById('inputAdmin').value;

	if(valorComun.indexOf(",")==-1) {
		document.getElementById('inputComun').value = valorComun+',00';
		valorComun = parseFloat(valorComun+'.00');
	} else {
		valorComun = parseFloat(valorComun.replace(",","."));
	}
	
	if(valorEsc85.indexOf(",")==-1) {
		document.getElementById('inputEsc85').value = valorEsc85+',00';
		valorEsc85 = parseFloat(valorEsc85+'.00');
	} else {
		valorEsc85 = parseFloat(valorEsc85.replace(",","."));
	}
	
	if(valorEsc91.indexOf(",")==-1) {
		document.getElementById('inputEsc91').value = valorEsc91+',00';
		valorEsc91 = parseFloat(valorEsc91+'.00');
	} else {
		valorEsc91 = parseFloat(valorEsc91.replace(",","."));
	}
	
	if(valorEsc95.indexOf(",")==-1) {
		document.getElementById('inputEsc95').value = valorEsc95+',00';
		valorEsc95 = parseFloat(valorEsc95+'.00');
	} else {
		valorEsc95 = parseFloat(valorEsc95.replace(",","."));
	}
	
	if(valorEscMV.indexOf(",")==-1) {
		document.getElementById('inputEscMV').value = valorEscMV+',00';
		valorEscMV = parseFloat(valorEscMV+'.00');
	} else {
		valorEscMV = parseFloat(valorEscMV.replace(",","."));
	}
	
	if(valorEscPK.indexOf(",")==-1) {
		document.getElementById('inputEscPK').value = valorEscPK+',00';
		valorEscPK = parseFloat(valorEscPK+'.00');
	} else {
		valorEscPK = parseFloat(valorEscPK.replace(",","."));
	}
	
	if(valorEscAd.indexOf(",")==-1) {
		document.getElementById('inputAdmin').value = valorEscAd+',00';
		valorEscAd = parseFloat(valorEscAd+'.00');
	} else {
		valorEscAd = parseFloat(valorEscAd.replace(",","."));
	}
	
	var sum = valorComun+valorEsc85+valorEsc91+valorEsc95+valorEscMV+valorEscPK+valorEscAd;
	sum = sum.toFixed(2);
	sum = sum.toString();
	sum = sum.replace(".",",");
	
	document.getElementById('inputTotal').value = sum;
}

function simulaGastoAdmin() {
	
	var inputAdminCasa = document.getElementById('inputAdminCasa').value;
	var inputAdminPark = document.getElementById('inputAdminPark').value;
	
	inputAdminCasa = parseFloat(inputAdminCasa.replace(",","."));
	inputAdminPark = parseFloat(inputAdminPark.replace(",","."));
	
	var calc = (inputAdminCasa*26) + (inputAdminPark*35);
	calc = calc.toFixed(2);
	calc = calc.toString();
	calc = calc.replace(".",",");
	
	document.getElementById('inputAdmin').value = calc;
}

function activaSimulador() {
	
	var ver = document.getElementById('tablaSimulacion').style.display;
	if(ver=='none') {
		document.getElementById('tablaSimulacion').style.display = '';
	} else {
		document.getElementById('tablaSimulacion').style.display = 'none';
	}
}

function recupera(gComun,g85,g91,g95,gMV,gPK) {
	
	document.getElementById('inputComun').value = (gComun/100).toFixed(2).replace(".",",");
	document.getElementById('inputEsc85').value = (g85/100).toFixed(2).replace(".",",");
	document.getElementById('inputEsc91').value = (g91/100).toFixed(2).replace(".",",");
	document.getElementById('inputEsc95').value = (g95/100).toFixed(2).replace(".",",");
	document.getElementById('inputEscMV').value = (gMV/100).toFixed(2).replace(".",",");
	document.getElementById('inputEscPK').value = (gPK/100).toFixed(2).replace(".",",");

	document.getElementById('inputAdminCasa').value = '76,38';
	document.getElementById('inputAdminPark').value = '30,55';
}