<?php
/**
 * Clase que maneja las newsletters de la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://alsina.juanmacordoba.com/license BSD Licence
 * @link      http://alsina.juanmacordoba.com
 */
class newsletter extends bbdd
{
    /**
    * Envía un comentario por mail a los destinatarios indicados
    *
    * @param string $user          Usuario que escribe el comentario
    * @param string $comentario    Comentario que se envía por mail
    * @param array  $destinatarios Mails de los destinatarios
    *
    * @return bool  Resultado del envío de mails
    */
    function sendNewsletter($user, $comentario, $destinatarios)
    {
        // tratamos el comentario
        $coment = nl2br($comentario);
        $coment = str_replace("\'", "'", $coment);
        // destinatario principal >> YO
        $addressTo = "jmcordoba@gmail.com";
        // asunto
        $subject = "Nou missatge publicat per un veï de la Comunitat";
        // construimos el cuerpo del mail
        $body  = '';
        $body .= '<table cellspacing="0" cellpadding="0" style="width:100%;height:100%;margin:0px;padding:0px;border: 1px solid #259DD5;">';
        // cabecera
        $body .= '<tr style="width:100%;height:25%;background-color:#31A4D9;">';
        $body .= '<td style="padding:20px;color:#ffffff;font-size:16px;">';
        $body .= 'Comunitat de Propietaris Carrer Alsina - Maria Vidal';
        $body .= '</td>';
        $body .= '</tr>';
        // presentacion y contenido
        $body .= '<tr style="width:100%;height:50%;background-color:#ffffff;" valign="top">';
        $body .= '<td style="padding:20px;color:#444444;font-size:14px;" valign="top">';
        $body .= 'El següent missatge ha sigut escrit per <b>'.$user.'</b> a la web de la Comunitat de Veïns:<br><br>';
        $body .= '<i>"'.utf8_encode($coment).'"</i>';
        $body .= '</td>';
        $body .= '</tr>';
        // pie
        $body .= '<tr style="width:100%;height:25%;background-color:#f1f1f1;" valign="top"">';
        $body .= '<td style="padding:20px;color:#444444;font-size:14px;" valign="top">';
        $body .= 'Comunitat de Propietaris Carrer Alsina - Maria Vidal ';
        $body .= '<br><br>';
        $body .= 'Si vols accedir al web punxa <a href="http://alsina.juanmacordoba.com">aquí</a> o visita la pàgina http://alsina.juanmacordoba.com';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        // construimos cabecera
        $header  = "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html; charset=UTF-8\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n";
        $header .= "From: Comunitat de Propietaris Carrer Alsina - Maria Vidal <".$_COOKIE['user'].">\r\n";
        $header .= "Reply-To: Comunitat de Propietaris Carrer Alsina - Maria Vidal | Vilassar de Mar <".$_COOKIE['user'].">\r\n";
        // anyadimos mails extras
        $to = array("jmcordoba@icsoftware.es","jmcordobah@gmail.com","arig82@gmail.com","jm@juanmacordoba.com");
        // anyadimos todos los vecinos de los que disponemos mail
        // para cada destinatario
        for ($v=0; $v<count($destinatarios); $v++) {
            $to[] = $destinatarios[$v]['mail'];
        }
        // anyadimos mails de vecinos como destinatarios en copia
        $header .= 'BCC: '. implode(",", $to) . "\r\n";
        // enviamos el mail y devolvemos resultado
        return mail($addressTo, $subject, $body, $header);
    }
}