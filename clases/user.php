<?php
/**
 * Clase que modela al usuario de la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class user extends bbdd
{
    /**
    * Busca al usuario por sus credenciales en la tabla de usuarios
    *
    * @param string $user Identificador del usuario
    * @param string $pass Password del usuario
    *
    * @return bool true or false
    */
    function getUser($user,$pass)
    {
		// inicializamos
        $result = false;
        // obtenemos el handle de la bbdd
		$handle = $this->getHandle();
        // construimos query
        $sql = "select * from `alsina_usuarios` where user='$user' and pass='$pass'";
		// seleccionamos los registros que coincidan con la sentencia
		$res = $handle->query($sql);
        // guardo lo de la query
        $this->writeLog($sql);
		// solo si hemos encontrado 1 resultado
		if($res->rowCount()==1) {
			// por cada resultado
			foreach($res as $row) {
				// guardamos cookies
				$this->setCookie("nomb",  $row["nomb"]);
                $this->setCookie("user",  $row["user"]);
				$this->setCookie("pass",  $row["pass"]);
				$this->setCookie("admin", $row["admin"]);
			}
			// identificamos el resultado como valido
			$result = true;
		}
        // devolvemos resultado
		return $result;
	}

    /**
     *
     */
    public function writeLog($sql='')
    {
        // si la query NO es vacia
        if($sql) {
            // construyo el log
            $content = date("dmYHis")." ".$sql."\n";
            // guardamos contenido generado
            $ar = fopen("public/files/access.log", "a+");
            fputs($ar, $content);
            fclose($ar);
        }
    }
    
    /**
    * Incrementa en 1 el contador de visitas para el usuario determinado
    *
    * @param string $user Identificador del usuario
    *
    * @return bool true or false
    */
	function setVisita($user)
    {
		// obtenemos el handle de la bbdd
		$handle = $this->getHandle();
		// actualizamos en bbdd y devolvemos el resultado de la operacion
		return $handle->exec("UPDATE `alsina_usuarios` SET visitas = visitas + 1 where user = '$user';");
	}
    
    /**
     * Guarda cookie
     */
    public function setCookie($name, $value)
    {
        // si el nombre de la variable No es vacio
        if($name)
        {
            // guardamos cookie
            setcookie($name, $value, time()+3600, '/', 'alsina.juanmacordoba.com');
        }
    }
    
    /**
     * Borramos cookie
     */
    public function delCookie($name)
    {
        // si el nombre de la variable No es vacio
        if($name)
        {
            // borramos cookie
            setcookie($name, '', time()-3600, '/', 'alsina.juanmacordoba.com');
        }
    }
    
    /**
     * Desconectamos usuario y redirigimos a inicio
     */
    public function disconnect()
    {
        // guardamos las cookies de las credenciales del usuario
        $this->delCookie('user');
        $this->delCookie('nomb');
        $this->delCookie('pass');
        $this->delCookie('admin');
        //
        ob_clean();
        header('Location:http://alsina.juanmacordoba.com/');
        //
        die();
    }
}