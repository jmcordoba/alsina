<?php
/**
 * Clase que maneja los comentarios de la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class comentario extends bbdd
{
    // propiedades
    public $id         = '';
	public $user       = '';
	public $fecha      = '';
	public $comentario = '';
    public $aComen;
 
    /**
    * Obtiene los comentarios que existen registrados
    *
    * @param void
    *
    * @return array Listado de Comentarios (arrays)
    */
	function getComentarios()
    {
		// obtenemos el handle de la bbdd
        $handle = $this->getHandle();
		// montamos y ejecutamos la query para obtener los comentarios
		$res = $handle->query("select id,user,fecha,comentario from `alsina_comentarios` order by fecha desc ");
        // por cada uno de los comentarios
        foreach($res as $row) {
            // los recojemos, adaptamos y montamos en un array
            $res = array(
                "id"         => utf8_encode($row["id"]),
                "user"       => utf8_encode($row["user"]),
                "fecha"      => utf8_encode($row["fecha"]),
                "comentario" => utf8_encode($row["comentario"])
                );
            // acumulamos el array del comentario en el array de resultados
            $aComen[] = $res;
        }
        // devolvemos el array de resultados
        return $aComen;
	}
    
    /**
     * Introducimos comentario en la bbdd
     * 
     * @param string $comentario
     * 
     * @return bool Resultado de la ejecución de la query
     */
    function setComentario($comentario)
    {
        // generamos el id unico
		$hoy = getdate();
        $id  = $hoy[0];
        // obtenemos el usuario
        $user = $_COOKIE['nomb'];
        // obtenemos la fecha en la que se guarda el comentario
        $fecha = $this->getId();
        // obtenemos el handle
        $handle = $this->getHandle();
        // insertamos el comentario y devolvemos resultado de la operación
		return $handle->exec("INSERT INTO `alsina_comentarios` (`id`,`user`,`fecha`,`comentario` ) VALUES ('$id','$user','$fecha','$comentario');");
    }
}