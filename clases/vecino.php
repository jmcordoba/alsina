<?php
/**
 * Clase que maneja los datos de los vecinos registrados en la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class vecino extends bbdd
{
    // propiedades
    public $id   = '';
	public $piso = '';
	public $nomb = '';
	public $telf = '';
    public $mail = '';
    public $aVecinos = array("id","piso","nomb","telf","mail");
    
    /**
    * Obtiene un listado con los datos de todos los vecinos registrados
    *
    * @param void
    *
    * @return array Listado de datos de los vecinos registrados
    */
	function getVecinos() {
		// inicializamos
        $aVecinos = null;
        // obtenemos el handle
		$handle = $this->getHandle();
		// ejecutamos la query
		$res = $handle->query("select id,piso,nomb,telf,mail from `alsina_vecinos`");
        // por cada resultado
        foreach($res as $row) {
            // guardamos el resultado de cada fila en un array
            $res = array(
                "id"   => utf8_encode($row["id"]),
                "piso" => utf8_encode($row["piso"]),
                "nomb" => utf8_encode($row["nomb"]),
                "telf" => utf8_encode($row["telf"]),
                "mail" => utf8_encode($row["mail"])
                );
            // lo agrupamos en una lista de arrays
            $aVecinos[] = $res;
        }
        // devolvemos el contenedor
        return $aVecinos;
	}
    
    /**
    * Obtiene un listado con los mails de todos los vecinos registrados
    * para enviarles la newsletter
    *
    * @param void
    *
    * @return array Listado de datos de los vecinos registrados
    */
    function getVecinosToNewsletter() {
		// inicializamos
        $aVecinos = null;
        // obtenemos el handle
		$handle = $this->getHandle();
		// ejecutamos la query
		//$res = $handle->query("select mail from `alsina_vecinos`");
        $res = $handle->query("select user from `alsina_usuarios`");
        // por cada resultado
        foreach($res as $row) {
            // si el campo mail NO es vacio
            if (strlen($row["user"])>0) {
                // añadimos el mail al array
                $aVecinos[] = utf8_encode($row["user"]);
            }
        }
        // devolvemos el array de mails
        return $aVecinos;
	}
}