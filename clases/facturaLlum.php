<?php
/**
 * Clase que maneja las facturas de luz de la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class facturaLlum extends bbdd
{
    /**
    * Obtiene una factura determinada en función del año, mes y escalera
    *
    * @param string $any    Año de la factura
    * @param string $mes    Mes de la factura
    * @param string $escala Escalera 
    *
    * @return array Datos de la factura
    */
	function getFactura($any,$mes,$escala)
    {
		// inicializamos
		$result = false;
		// obtenemos handle
		$handle = $this->getHandle();
		// seleccionamos factura
		$res = $handle->query("select * from `alsina_llum` where any='$any' and mes='$mes' and escala='$escala'");
		// si hemos encontrado una sola factura >> ok
		if($res->rowCount()==1) {
			// la guardamos
			$result = $res;
		}
		// devolvemos resultado
		return $result;
	}
	
    /**
    * Comprueba que la factura que queremos introducir NO exista
    *
    * @param string $any    Año de la factura
    * @param string $mes    Mes de la factura
    * @param string $escala Escalera 
    *
    * @return array Datos de la factura
    */
	function getFacturaDuplicada($any,$mes,$escala)
    {
		// inicializamos
		$result = true;
		// obtenemos handle
		$handle = $this->getHandle();
		// seleccionamos factura
		$res = $handle->query("select * from `alsina_llum` where any='$any' and mes='$mes' and escala='$escala'");
		// si hemos encontrado una sola factura >> ok
		if($res->rowCount()==0) {
			// la guardamos
			$result = false;
		}
		// devolvemos resultado
		return $result;
	}
	
    /**
    * Introduce los datos de la factura en la bbdd
    *
    * @param string $any     Año de la factura
    * @param string $mes     Mes de la factura
    * @param string $escala  Escalera 
    * @param string $consum  Consumo en KWh
    * @param string $factura Importe de la factura en Euros
    * @param string $ruta    Ruta donde se encuentra el PDF de la factura
    *
    * @return bool  Devuelve el resultado de la ejecución de la query
    */
	function setFactura($any,$mes,$escala,$consum,$factura,$ruta)
    {
		// generamos el id unico
		if($escala=='parking') {
			$id = $any.$mes.'00';
		} else if($escala=='mvidal') {
			$id = $any.$mes.'01';
		} else {
			$id = $any.$mes.$escala;
		}
		
		// obtenemos handle
		$handle = $this->getHandle();
		// insertamos la factura
		$res = $handle->exec("INSERT INTO `alsina_llum` (`id`,`any`,`mes`,`escala`,`consum`,`factura`,`enlace` ) VALUES ('$id','$any','$mes','$escala','$consum','$factura','$ruta');");
	}
}