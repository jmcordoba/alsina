<?php
/**
 * Clase que maneja los presupuestos de la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class presupuesto extends bbdd{

    // propiedades
	public $any     = '';
	public $comunes = '';
	public $esc85   = '';
	public $esc91   = '';
	public $esc95   = '';
	public $escMV   = '';
	public $escPK   = '';
	public $admin   = '';
	public $total   = '';	
	
    /**
    * Obtiene unpresupuesto en función del año
    *
    * @param string $any Año del presupuesto
    *
    * @return bool Indicamos el éxito de la operación
    */
	function getPresupuesto($any)
    {
        // inicializamos
		$result = false;
        // obtenemos el handle de la bbdd
		$handle = $this->getHandle();
		// ejecutamos query
		$res = $handle->query("select * from `alsina_presupuestos` where any='$any' ");
		// si solo encontramos 1 resultado
		if($res->rowCount()==1) {
			// por cada resultado
			foreach($res as $row) {
				// obtenemos datos y los guardamos en las propiedades del objeto
				$this->any     = $row["any"];
				$this->comunes = str_replace(",","",$row["comunes"]);
				$this->esc85   = str_replace(",","",$row["85"]);
				$this->esc91   = str_replace(",","",$row["91"]);
				$this->esc95   = str_replace(",","",$row["95"]);
				$this->escMV   = str_replace(",","",$row["mvidal"]);
				$this->escPK   = str_replace(",","",$row["parking"]);
				$this->admin   = str_replace(",","",$row["admin"]);
				$this->total   = str_replace(",","",$row["total"]);
			}
			// indicamos que hemos tenido éxito
			$result = true;
		}
        // devolvemos resultado
		return $result;
	}
	
    /**
    * Obtiene los presupuestos registrados
    *
    * @param void
    *
    * @return array Lista de presupuestos
    */
	function getListaPresupuestos()
    {
        // obtenemos el handle de la bbdd
		$handle = $this->getHandle();
		// ejecutamos la query para obtener los presupuestos registrados
		$res = $handle->query("select any,comunes,85,91,95,mvidal,parking,admin,total from `alsina_presupuestos` order by any desc ");
		// por cada uno de los registros encontrados
        foreach($res as $row) {
            // obtenemos los valores de cada campo
            $res = array(
                "any"     => $row["any"],
                "comunes" => $row["comunes"],
                "esc85"   => $row["85"],
                "esc91"   => $row["91"],
                "esc95"   => $row["95"],
                "escMV"   => $row["mvidal"],
                "escPK"   => $row["parking"],
                "admin"   => $row["admin"],
                "total"   => $row["total"]
                );
            // acumulamos el resultado en un array
            $aPresup[] = $res;
        }
        // devolvemos el array de resultados
        return $aPresup;
	}
    
    /**
    * Obtiene los datos de una propiedad determinada
    *
    * @param string $idProp Identificador de la propiedad
    *
    * @return array Lista de valores de una propiedad determinada
    */
	function getPropiedad($idProp)
    {
        // inicializamos
        $row = false;
		// obtenemos el handle de la bbdd
		$handle = $this->getHandle();		
		// ejecutamos query para obtener valores de la propiedad
        $res = $handle->query("select * from `alsina_vecinos` where id='$idProp' ");
		// si solo encontramos 1 resultado
		if($res->rowCount()==1) {
			// por cada resultado
            foreach($res as $row) {
				// devolvemos el resultado
                return $row;
			}
		}
	}
	
    /**
    * Obtiene la cuota anual real a pagar en función de las propiedades indicadas
    *
    * @param string $piso     Identificador del piso
    * @param string $parking  Identificador del parking
    * @param string $traster  Identificador del trastero
    * @param string $parkingm Identificador del parking de motos
    *
    * @return string Cuota anual resultante
    */
	function getAnualidad($piso,$parking,$traster,$parkingm,$any)
    {
		// inicializamos
		$parcialPiso     = 0;
		$parcialParking  = 0;
		$parcialTraster  = 0;
		$parcialParkingM = 0;
		// cargamos los datos del presupuesto aprobado
		$oPres = $this->getPresupuesto($any);
		// si hay un piso indicado
		if($piso!='No') {
			// obtenemos el objeto piso
			$oPiso = $this->getPropiedad($piso);
			// obtenemos los datos del objeto
			$coef_total   = str_replace(",","",$oPiso['coef_total']);
			$coef_parcial = str_replace(",","",$oPiso['coef_parcial']);
			$gasto1       = str_replace(",","",$oPiso['gasto1']);
			$gasto2       = str_replace(",","",$oPiso['gasto2']);
			$gasto3       = str_replace(",","",$oPiso['gasto3']);
			$gasto4       = str_replace(",","",$oPiso['gasto4']);
			$gasto5       = str_replace(",","",$oPiso['gasto5']);
			$gasto6       = str_replace(",","",$oPiso['gasto6']);
			$gasto7       = str_replace(",","",$oPiso['gasto7']);
			// gastos comunes * coeficiente total de la vivienda
			if($gasto1==true) {
				$parcialPiso += ( $this->comunes * $coef_total ) / 1000000;
			}
			// gastos de la escalera * coeficiente total de la escalera
			if($gasto2==true) {
				$parcialPiso += ( $this->esc85 * $coef_parcial ) / 1000000;
			}
			if($gasto3==true) {
				$parcialPiso += ( $this->esc91 * $coef_parcial ) / 1000000;
			}
			if($gasto4==true) {
				$parcialPiso += ( $this->esc95 * $coef_parcial ) / 1000000;
			}
			if($gasto5==true) {
				$parcialPiso += ( $this->escMV * $coef_parcial ) / 1000000;
			}
			// cuota del administrador hacia el la vivienda
			if($gasto7==true) {
				$parcialPiso += 76.38;
			}
		}
        // si hay parking de coches indicado
		if($parking!='No') {
			// obtenemos el objeto piso
			$oParking = $this->getPropiedad($parking);
			// obtenemos los datos del objeto
			$coef_total   = 0.5133;
			$coef_parcial = 2.38;
			// gastos comunes * coeficiente total que le toca (0,5133%)
			$parcialParking += ( $this->comunes * $coef_total ) / 10000;
			// gastos del parking * coeficiente del parking (2,38%)
			$parcialParking += ( $this->escPK * $coef_parcial ) / 10000;
			// cuota del administrador hacia el parking
			$parcialParking += 30.55;
		}
		// si hay trastero indicado
		if($traster!='No') {
            // inicializamos
			$coef_total   = 0.1711;
			$coef_parcial = 0.79;
			// gastos comunes * coeficiente total que le toca (0,1711%)
			$parcialTraster += ( $this->comunes * $coef_total ) / 10000;
			// gastos del trastero * coeficiente del trastero (0,79%)
			$parcialTraster += 0;
			// cuota del administrador hacia el trastero
			$parcialTraster += 0;
		}
		// si hay parking de motos
		if($parkingm!='No') {
            // inicializamos
			$coef_total   = 0.1283;
			$coef_parcial = 0.60;
			// gastos comunes * coeficiente total que le toca (0,1283%)
			if($gasto1==true) {
				$parcialParkingM += ( $this->comunes * $coef_total ) / 10000;
			}
			// gastos del parking de motos * coeficiente del parking de motos (0,60%)
			$parcialParkingM += 0;
			// cuota del administrador hacia el trastero
			$parcialParkingM += 0;
		}		
		// devolvemos la suma de las cuotas en función de las propiedades
		return $parcialPiso+$parcialParking+$parcialTraster+$parcialParkingM;
	}
	
    /**
    * Obtiene la cuota anual simulada a pagar en función de las propiedades indicadas
    *
    * @param string $piso       Identificador del piso
    * @param string $parking    Identificador del parking
    * @param string $traster    Identificador del trastero
    * @param string $parkingm   Identificador del parking de motos
    * @param string $gComun     Gasto común
    * @param string $gEsc85     Gasto de la escalera 85
    * @param string $gEsc91     Gasto de la escalera 91
    * @param string $gEsc95     Gasto de la escalera 95
    * @param string $gEscMV     Gasto de la escalera Maria Vidal
    * @param string $gEscPK     Gasto del Parking
    * @param string $gAdmin     Gasto en Honorarios Administrativos
    * @param string $gAdminCasa Gasto total del Administrador en Hogares
    * @param string $gAdminPark Gasto total del Administrador en Parking
    * 
    * @return string Cuota anual resultante
    */
	function getAnualidadSimulada($piso,$parking,$traster,$parkingm,$gComun,$gEsc85,$gEsc91,$gEsc95,$gEscMV,$gEscPK,$gAdmin,$gAdminCasa,$gAdminPark,$any)
    {
		// inicializamos
		$parcialPiso     = 0;
		$parcialParking  = 0;
		$parcialTraster  = 0;
		$parcialParkingM = 0;
		// cargamos los datos del presupuesto aprobado
		$this->any     = $any;
		$this->comunes = str_replace(",","",$gComun);
		$this->esc85   = str_replace(",","",$gEsc85);
		$this->esc91   = str_replace(",","",$gEsc91);
		$this->esc95   = str_replace(",","",$gEsc95);
		$this->escMV   = str_replace(",","",$gEscMV);
		$this->escPK   = str_replace(",","",$gEscPK);
		$this->admin   = str_replace(",","",$gAdmin);
		// si hemos indicado algún piso
		if($piso!='No') {
			// obtenemos el objeto piso
			$oPiso = $this->getPropiedad($piso);
			// obtenemos los datos del objeto
			$coef_total   = str_replace(",","",$oPiso['coef_total']);
			$coef_parcial = str_replace(",","",$oPiso['coef_parcial']);
			$gasto1       = str_replace(",","",$oPiso['gasto1']);
			$gasto2       = str_replace(",","",$oPiso['gasto2']);
			$gasto3       = str_replace(",","",$oPiso['gasto3']);
			$gasto4       = str_replace(",","",$oPiso['gasto4']);
			$gasto5       = str_replace(",","",$oPiso['gasto5']);
			$gasto6       = str_replace(",","",$oPiso['gasto6']);
			$gasto7       = str_replace(",","",$oPiso['gasto7']);
			// gastos comunes * coeficiente total de la vivienda
			if($gasto1==true) {
				$parcialPiso += ( $this->comunes * $coef_total ) / 1000000;
			}
			// gastos de la escalera * coeficiente total de la escalera
			if($gasto2==true) {
				$parcialPiso += ( $this->esc85 * $coef_parcial ) / 1000000;
			}
			if($gasto3==true) {
				$parcialPiso += ( $this->esc91 * $coef_parcial ) / 1000000;
			}
			if($gasto4==true) {
				$parcialPiso += ( $this->esc95 * $coef_parcial ) / 1000000;
			}
			if($gasto5==true) {
				$parcialPiso += ( $this->escMV * $coef_parcial ) / 1000000;
			}
			// cuota del administrador hacia el la vivienda
			$parcialParking += $gAdminCasa;
		}
		// si hemos indicado algún parking
		if($parking!='No') {
			// obtenemos el objeto piso
			$oParking = $this->getPropiedad($parking);
			// obtenemos los datos del objeto
			$coef_total   = 0.5133;
			$coef_parcial = 2.38;
			// gastos comunes * coeficiente total que le toca (0,5133%)
			$parcialParking += ( $this->comunes * $coef_total ) / 10000;
			// gastos del parking * coeficiente del parking (2,38%)
			$parcialParking += ( $this->escPK * $coef_parcial ) / 10000;
			// cuota del administrador hacia el parking
			$parcialParking += $gAdminPark;
		}
		// si hemos indicado algún trastero
		if($traster!='No') {
            // inicializamos
			$coef_total   = 0.1711;
			$coef_parcial = 0.79;
			// gastos comunes * coeficiente total que le toca (0,1711%)
			$parcialTraster += ( $this->comunes * $coef_total ) / 10000;
			// gastos del trastero * coeficiente del trastero (0,79%)
			$parcialTraster += 0;
			// cuota del administrador hacia el trastero
			$parcialTraster += 0;
		}
		// si hemos indicado algun parking de motos
		if($parkingm!='No') {
            // inicializamos
			$coef_total   = 0.1283;
			$coef_parcial = 0.60;
			// gastos comunes * coeficiente total que le toca (0,1283%)
			if($gasto1==true) {
				$parcialParkingM += ( $this->comunes * $coef_total ) / 10000;
			}
			// gastos del parking de motos * coeficiente del parking de motos (0,60%)
			$parcialParkingM += 0;
			// cuota del administrador hacia el trastero
			$parcialParkingM += 0;
		}		
		// devolvemos la suma de las cuotas en función de las propiedades
		return $parcialPiso+$parcialParking+$parcialTraster+$parcialParkingM;
	}
    
    /**
    * Busca un presupuesto para el año indicado
    *
    * @param string $any Año del presupuesto
    * 
    * @return bool Resultado de la búsqueda
    */
    function existePresupuesto ($any)
    {
        // inicializamos
        $result = false;
        // obtiene el handle de la bbdd
		$handle = $this->getHandle();
		// ejecutamos la query
		$res = $handle->query("select * from `alsina_presupuestos` where any='$any' ");
		// si encontramos algun presupuesto
		if ($res->rowCount()>0) {
			// indicamos que ha sido encontrado
			$result = true;
		}
        // devolvemos resultado
		return $result;
    }
    
    /**
    * Crea un presupuesto en la bbdd con importes 0
    *
    * @param string $any Año del presupuesto
    * 
    * @return bool Resultado de la creación del presupuesto
    */
    function creaPresupuesto ($any)
    {
        // inicializamos
        $result = false;
        // obteiene el handle de la bbdd
		$handle = $this->getHandle();
		// construimos sentencia de la estadistica
        $texto  = "INSERT INTO `alsina_presupuestos` (`any`,`comunes`,`85`,`91`,`95`,`mvidal`,`parking`,`admin`,`total`) ";
        $texto .= "VALUES ('$any','0,00','0,00','0,00','0,00','0,00','0,00','0,00','0,00')";
        // insertamos la estadistica
        return $handle->exec($texto);
    }
    
    /**
    * Elimina un presupuesto determinado por el año
    *
    * @param string $any Año del presupuesto
    * 
    * @return bool Resultado de la creación del presupuesto
    */
    function eliminaPresupuesto ($any)
    {
        // inicializamos
        $result = false;
        // obtenemos el handle de la bbdd
		$handle = $this->getHandle();
		// construimos sentencia de la estadistica
        $texto  = "DELETE FROM `alsina_presupuestos` WHERE any='".$any."' LIMIT 1";
        // insertamos la estadistica
        return $handle->exec($texto);
    }
}