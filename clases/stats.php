<?php
/**
 * Clase que maneja las estaditicas de los accesos a la aplicación
 *
 * PHP version 5
 *
 * @category  Clases
 * @package   Default
 * @author    JuanMa Córdoba <jmcordoba@gmail.com>
 * @copyright 2013-2014 Navegalia
 * @license   http://www.navegalia.net/alsina/license BSD Licence
 * @link      http://www.navegalia.net/alsina
 */
class estadistica extends bbdd
{
	/**
    * Inserta estadistica en la bbdd con los datos del usuario que nos visita
    *
    * @param void
    *
    * @return bool Indicamos el éxito de la operación
    */
	function insertaEstadistica()
    {
        // inicializamos salida
        $result = false;
        // obtenemos link con la bbdd
        $handle = $this->getHandle();
        // si es un nuevo usuario visitante en la pagina
        if($this->ifNuevoUsuario() == 0) {
            // generamos y/u obtenemos datos estadisticos
            $numero             = $this->getId();
			$remoteIP           = $_SERVER['REMOTE_ADDR'];
			$userAgent          = $_SERVER['HTTP_USER_AGENT'];
			$httpAcceptLanguage = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
			$scriptName         = $_SERVER["SCRIPT_NAME"];
            // construimos sentencia de la estadistica
			$texto  = "INSERT INTO `alsina_analytics` (`id`,`fecha`,`hora`,`ip`,`navegador`,`so`,`idioma`,`charset`) ";
			$texto .= "VALUES (" ;
			$texto .= "'$numero',";
			$texto .= "'".date('y-m-d')."',";
			$texto .= "'".date('H.i:s')."',";
			$texto .= "'$remoteIP',";
			$texto .= "'".$this->encuentraNavegador($userAgent)."',";
			$texto .= "'".$this->encuentraSO($userAgent)."',";
			$texto .= "'$httpAcceptLanguage',";
			$texto .= "'$scriptName')";
            // insertamos la estadistica
            $result = $handle->exec($texto);
		}
        // devolvemos el resultado de la operación
        return $result;
	}
	
    /**
    * Obtiene la descripción del navegador del usuario visitante en función
    * del USER_AGENT
    *
    * @param string $agente $_SERVER['HTTP_USER_AGENT'] del visitante
    *
    * @return string $navegador Descripción del navegador del usuario visitante
    */
	function encuentraNavegador($agente)
    {
		// internet explorer
		if(stripos($agente,"MSIE")){
			$navegador = substr($agente,stripos($agente,"MSIE"));
			$navegador = explode(";",$navegador);
			$navegador = $navegador[0];
		}
		// chrome
		else if(stripos($agente,"Chrome")){
			$navegador = substr($agente,stripos($agente,"Chrome"));
			$navegador = explode(" ",$navegador);
			$navegador = $navegador[0];
			$navegador = substr($navegador,0,stripos($navegador,"."));
		}
		// firefox
		else if(stripos($agente,"Firefox")){
			$navegador = substr($agente,stripos($agente,"Firefox"));
			$navegador = explode(" ",$navegador);
			$navegador = $navegador[0];
			$navegador = substr($navegador,0,stripos($navegador,"."));
		}
		// iphone - safari
		else if(stripos($agente,"iPhone OS")) {
			$navegador = "Safari - iphone";
		}
		// bot
		else if(stripos($agente,"bot") or stripos($agente,"spider") or stripos($agente,"crawler")) {
			$navegador = "bot";
		}
		// si no lo encontramos soltamos todo el user_agent
		else {
            $navegador = $agente;
        }
		// devolvemos resultado
		return $navegador;
	}

    /**
    * Obtiene la descripción del Sistema Operativo del usuario visitante en función
    * del USER_AGENT
    *
    * @param string $agente $_SERVER['HTTP_USER_AGENT'] del visitante
    *
    * @return string $navegador Descripción del navegador del usuario visitante
    */
	function encuentraSO($agente)
    {
		// Microsoft Windows
        if(stripos($agente,"Windows NT 6.2"))       { 
            $sistemaOperativo = "Windows 8"; 
        }
        else if(stripos($agente,"Windows NT 6.1"))  { 
            $sistemaOperativo = "Windows 7";
        }
        else if(stripos($agente,"Windows NT 6.0"))  { 
            $sistemaOperativo = "Windows Vista";
        }
        else if(stripos($agente,"Windows NT 5.2"))  { 
            $sistemaOperativo = "Windows Server 2003";
        }
        else if(stripos($agente,"Windows NT 5.1"))  { 
            $sistemaOperativo = "Windows XP"; 
        }
        else if(stripos($agente,"Windows NT 5.01")) { 
            $sistemaOperativo = "Windows 2000 SP1"; 
        }
        else if(stripos($agente,"Windows NT 5.0"))  { 
            $sistemaOperativo = "Windows 2000"; 
        }
        else if(stripos($agente,"Windows NT 4.0"))  { 
            $sistemaOperativo = "Windows NT 4.0"; 
        }
        else if(stripos($agente,"Win 9x 4.90"))     { 
            $sistemaOperativo = "Windows Me"; 
        }
        else if(stripos($agente,"Windows 98"))      { 
            $sistemaOperativo = "Windows 98";
        }
        else if(stripos($agente,"Windows 95"))      { 
            $sistemaOperativo = "Windows 95";
        }
        else if(stripos($agente,"Windows CE"))      { 
            $sistemaOperativo = "Windows CE";
        }
		// iphone
		else if(stripos($agente,"iPhone OS")) {
			$so = substr($agente,stripos($agente,"iPhone OS"));
			$so = explode(" ",$so);
			$so = $so[0]." ".$so[1]." ".$so[2];
			$sistemaOperativo = $so;
		}
		// LINUX general
		else if(stripos($agente,"Linux") and stripos($agente,"Ubuntu")===false) {
			$sistemaOperativo = "Linux";
		}
		// Ubuntu
		else if(stripos($agente,"Ubuntu")) {
			$so = substr($agente,stripos($agente,"Ubuntu"));
			$so = explode(";",$so);
			$so = $so[0].substr(str_replace("rv:","",$so[2]),0,stripos(str_replace("rv:","",$so[2]),")"));
			$sistemaOperativo = $so;
		}
		// bot
		else if(stripos($agente,"bot") or stripos($agente,"spider") or stripos($agente,"crawler")) {
			$sistemaOperativo = "bot";
		}
		// si no lo encontramos soltamos todo el user_agent
		else {
            $sistemaOperativo = $agente;
        }
		// devolvemos resultado
		return $sistemaOperativo;
	}
	
    /**
    * Obtenemos el número de veces que el usuario visitante nos ha visitado hoy
    *
    * @param void
    *
    * @return int Número de veces que el usuario visitante nos ha visitado hoy
    */
	function ifNuevoUsuario()
    {
        // obtenemos el link con la bbdd
        $handle = $this->getHandle();
        // obtenemos IP y pagina visitada
        $remoteIP  = $_SERVER['REMOTE_ADDR'];
		$userAgent = $_SERVER["SCRIPT_NAME"];
        // consultamos si el usuario visitante ya ha visitado la pagina hoy
        $sql = "SELECT * FROM `alsina_analytics` where ip = '$remoteIP' and charset='$userAgent' and fecha=curdate()";
        $res = $handle->query($sql);
		// devolvemos el numero de registros encontardos
		return $res->rowCount();
	}
    
    /**
    * Inserta estadistica en la bbdd con los datos del usuario que no hemos permitido acceder
    *
    * @param void
    *
    * @return bool Indicamos el éxito de la operación
    */
	function insertaEstadisticaControlAcceso()
    {
        // inicializamos salida
        $result = false;
        // obtenemos link con la bbdd
        $handle = $this->getHandle();
        // si es un nuevo usuario visitante en la pagina
        if($this->ifNuevoUsuario() == 0) {
            // generamos y/u obtenemos datos estadisticos
            $numero             = $this->getId();
			$remoteIP           = $_SERVER['REMOTE_ADDR'];
			$userAgent          = $_SERVER['HTTP_USER_AGENT'];
			$httpAcceptLanguage = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
			$scriptName         = "CONTROL DE ACCESO ACTIVADO";
            // construimos sentencia de la estadistica
			$texto  = "INSERT INTO `alsina_analytics` (`id`,`fecha`,`hora`,`ip`,`navegador`,`so`,`idioma`,`charset`) ";
			$texto .= "VALUES (" ;
			$texto .= "'$numero',";
			$texto .= "'".date('y-m-d')."',";
			$texto .= "'".date('H.i:s')."',";
			$texto .= "'$remoteIP',";
			$texto .= "'".$this->encuentraNavegador($userAgent)."',";
			$texto .= "'".$this->encuentraSO($userAgent)."',";
			$texto .= "'$httpAcceptLanguage',";
			$texto .= "'$scriptName')";
            // insertamos la estadistica
            $result = $handle->exec($texto);
		}
        // devolvemos el resultado de la operación
        return $result;
	}
}