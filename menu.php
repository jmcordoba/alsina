<?php
/**
 * Controlador del menú de la aplicación
 * 
 * Si alguien accede directamente a esta página y no tiene usuario y password
 * correctamente guardado en COOKIES >> será redirigido a la página principal
 * 
 * Dependiendo de $_REQUEST['accion'] realizamos diferentes acciones
 */

// incluimos clases
require_once 'clases/bbdd.php';
require_once 'clases/stats.php';

// creamos y validamos usuario
$stat = new estadistica();
$stat->insertaEstadistica();

// control de credenciales
if( $_COOKIE['user']!='' and $_COOKIE['pass']!='' ) {
	
	// incluimos clases
	require_once 'clases/user.php';
	require_once 'clases/facturaLlum.php';
	require_once 'clases/presupuesto.php';
    require_once 'clases/vecino.php';
    require_once 'clases/comentario.php';
    require_once 'clases/newsletter.php';
	
	// creamos y validamos usuario
	$usuario = new user();
	$res     = $usuario->getUser($_COOKIE["user"], $_COOKIE["pass"]);
	
	// si usuario validado
	if($res==true) {
		// en función de la variable acción de la URL
        switch($_REQUEST['accion']) {
            // mostramos el backend de la aplicación
            case 'admin':
                include 'admin/index.php';
                break;
            // introducimos nueva factura
            case 'nuevaFactura':
                include 'mod/nuevaFactura.php';
                break;
            // introducimos nuevo comentario
            case 'nuevoComentario':
                include 'mod/nuevoComentario.php';
                break;
            // mostramos un PDF
            case 'showPDF':
                include 'vistas/showPDF.php';
                break;
            // mostramos una imagen
            case 'showJPG':
                include 'vistas/showJPG.php';
                break;
            // mostramos una imagen
            case 'showXLS':
                include 'vistas/showXLS.php';
                break;
            // mostramos las opciones de navegación de los vecinos
            default:
                include 'vistas/index.php';        
                break;
		}
	} else {
		// redirect
		ob_clean();
        header("Location:index.php?accion=error&user=error");
        die();
	}
} else {
    // redirect
	ob_clean();
    header("Location:index.php?accion=error&cookies=no");
    die();
}