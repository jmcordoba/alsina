<?php

/**
 * Controlador de la página principal
 */

// incluimos clases
require_once 'clases/bbdd.php';
require_once 'clases/stats.php';

//
$permiso = false;
$langs   = explode(",",$_SERVER['HTTP_ACCEPT_LANGUAGE']);

// por cada uno de los idiomas aceptados por el navegador
for($i=0;$i<count($langs);$i++) {
    // si encontramos el español
    if( stripos($langs,"es")!==false ) {
        // permiso concedido
        $permiso = true;
    }
}

// si tenemos permiso para continuar
if($permiso) {
    // en función de la variable "accion" de la URL
    switch($_REQUEST["accion"]) {
        // validamos credenciales
        case 'valida':
            require_once 'mod/validaUser.php';
            break;
        // pagina de inicio
        default:
            require_once 'vistas/static_index.php';
            break;
    }
}
else {
    // guardamos estadistica de control de acceso
    $stat = new estadistica();
    $stat->insertaEstadisticaControlAcceso();
}