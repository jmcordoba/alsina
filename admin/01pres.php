<style>
.button {
    width:            100%;
    min-width:        100px;
    height:           25px;
    padding:          2px;
    border:           1px solid rgb(58, 166, 0);
    background-color: rgb(108, 211, 53);
    background-image: -moz-linear-gradient(center top , rgb(108, 211, 53), rgb(58, 166, 0));
    color:            #ffffff;
    cursor:           pointer;
    border-radius:    3px;
}
.button_red {
    width:            100px;
    height:           25px;
    padding:          2px;
    border:           1px solid red;
    background-color: red;
    background-image: -moz-linear-gradient(center top , red, red);
    color:            #ffffff;
    cursor:           pointer;
    border-radius:    3px;
}
</style>

<script>
    function habilitaNewPre(){
        var nouPres = document.getElementById('nouPres');
        
        if(nouPres.style.display == 'none'){
            nouPres.style.display = '';
        } else {
            nouPres.style.display = 'none';
        }
    }

    function guardaNewPre(){
        // obtenim l'any
        var anyNouPres = document.getElementById('anyNewPres');
        // preguntem si existeix un pressupost per aquest any
        var result = makeRequest('mod/admin/01existePresupuesto.php?any='+anyNouPres.value).responseText;
        // tractem el resultat
        if (result==false) {
            // insertamos un nuevo registro en la tabla correspondiente
            var resul2 = makeRequest('mod/admin/01creaPresupuesto.php?any='+anyNouPres.value).responseText;
            // tractem resultat
            if (resul2==true) {
                // ok
                alert('Pressupost creat correctament, ara es regenerarà la pàgina.');
                // refrescamos la pagina actual
                location.reload();
            } else {
                // error
                alert('No ha sigut possible crear el presupost'+anyNouPres.value);
            }
        } 
        else if (result==true) {
            alert('Ja existeix el pressupost: '+anyNouPres.value);
        }
        else {
            alert(result);
        }
    }
    
    function deletePre(any){
        
        var alerta = 'Està segur que vol eliminar el pressupost de l\'any '+any+' ?\n';
        alerta    += 'Aixó comporta la eliminació de moltes dades importants.\n\n';
        alerta    += 'Confirmi si està realment segur.';
        
        if(confirm(alerta)) {
            
            // intentem eliminar pressupost
            var result = makeRequest('mod/admin/01deletePresupuesto.php?any='+any).responseText;
            // tractem resultat
            if (result==true) {
                // ok
                alert('Pressupost eliminat correctament, ara es regenerarà la pàgina.');
                // refrescamos la pagina actual
                location.reload();
            } else {
                // error
                alert('No ha sigut possible eliminar el presupost'+any);
            }
        }
    }
    
    function muestraPresupuesto(any) {
        // intentem eliminar pressupost
        var result = makeRequest('mod/admin/01getPresupuesto.php?any='+any).responseText;
        // rellenamos el espacio con el presupuesto
        document.getElementById('presupuesto').innerHTML = result;
    }
    
    function editPre(any){
        // mostramos el dialogo para introducir nuevo gasto al presupuesto
        document.getElementById('divPre').style.display    = 'block';
        document.getElementById('divPreAny').value         = any;
        document.getElementById('divPreAnySpan').innerHTML = any;
    }
    
    function guardaGasto(){
        // obtenemos los datos del gasto
        var any = document.getElementById('divPreAny').value;
        var par = document.getElementById('divPrePar').value;
        var con = document.getElementById('divPreCon').value;
        var eur = document.getElementById('divPreEur').value;
        
        if (par=='' || eur==''){
            alert('Es obligatorio indicar una partida y un concepto de gasto');
        } else {
            
            //alert(' any: '+any+'\n partida: '+par+'\n Concepto: '+con+'\n Euros: '+eur);
        }
    }
</script>

<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
            
            <table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr VALIGN="top">
					<td align="left" WIDTH="49%">
            
                        <div style="min-height:200px;max-height:200px;height:100%;overflow-x:hidden;overflow-y:scroll;border:1px solid #31A4D9;">
                            
                            <table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:0px solid #259DD5;border-radius:0px;" >

                                <tr valign="top" align="left" height="35" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                                    <td valign="middle" colspan="4" style="padding:5px;width:100%;"><b>Llista de pressupostos</b></td>
                                </tr>

                                <?php

                                // creamos y validamos usuario
                                $presu = new presupuesto();
                                $res   = $presu->getListaPresupuestos();

                                for ($i=0;$i<count($res);$i++) {
                                    ?>
                                    <tr valign="top" id="fila">
                                        <td align="left" style="padding:5px;width:80%;vertical-align: middle;" onclick="muestraPresupuesto('<?php echo $res[$i]['any']; ?>');">
                                            Pressupost <?php echo $res[$i]['any']; ?>
                                        </td>
                                        <td align="right" style="padding:5px;width:10%;">
                                            <input type="submit" class="button" value="Editar" onclick="editPre('<?php echo $res[$i]['any']; ?>');" title="Añade o modifica gastos para cada partida." />
                                        </td>
                                        <td align="right" style="padding:5px;width:10%;">
                                            <input type="submit" class="button_red" value="Eliminar" onclick="deletePre('<?php echo $res[$i]['any']; ?>');" title="Elimina el presupuesto y todos sus datos asociados."/>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            
                        </div>
                        
                        <div id="divPre" style="margin-top:10px;min-height:243px;max-height:243px;height:100%;border:1px solid #31A4D9;display:none;">
                            
                            <input type="hidden" id="divPreAny" value="" />
                            
                            <table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:0px solid #259DD5;border-radius:0px;" >
                                
                                <tr align="left" height="35" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                                    <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>PRESSUPOST <span id="divPreAnySpan"></span></b></td>
                                </tr>
                                <tr valign="top" style="padding: 10px;">
                                    <td align="left" style="background-color:#f1f1f1;color:#000000;padding:10px;width:20%;vertical-align:middle;">
                                        Partida
                                    </td>
                                    <td align="left" style="padding:5px;width:80%;">
                                        <select id="divPrePar" style="background-color:#ffffff;width:100%;height:30px;padding:5px 0px 5px 0px;border:1px solid #31A4D9;">
                                            <option selected>Tria una despesa</option>
                                            <option value="comun">Gastos comunes</option>
                                            <option value="esc85">Escalera 85</option>
                                            <option value="esc91">Escalera 91</option>
                                            <option value="esc95">Escalera 95</option>
                                            <option value="escMV">Maria Vidal</option>
                                            <option value="escPK">Parking</option>
                                            <option value="admin">Administrador</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr valign="top" style="padding: 10px;">
                                    <td align="left" style="background-color:#f1f1f1;color:#000000;padding:10px;width:20%;vertical-align:middle;">
                                        Concepto
                                    </td>
                                    <td style="padding:5px;width:80%;text-align:left;">
                                        <input id="divPreCon" type="text" value="" style="width:96%;height:20px;padding:5px;border:1px solid #31A4D9;" />
                                    </td>
                                </tr>
                                <tr valign="top" style="padding: 10px;">
                                    <td align="left" style="background-color:#f1f1f1;color:#000000;padding:10px;width:20%;vertical-align:middle;">
                                        Descripción
                                    </td>
                                    <td style="padding:5px;width:80%;text-align:left;">
                                        <input id="divPreDes" type="text" value="" style="width:96%;height:20px;padding:5px;border:1px solid #31A4D9;" />
                                    </td>
                                </tr>
                                <tr valign="top" style="padding: 10px;">
                                    <td align="left" style="background-color:#f1f1f1;color:#000000;padding:10px;width:20%;vertical-align:middle;">
                                        Dinero (€)
                                    </td>
                                    <td style="padding:5px;width:80%;text-align:left;">
                                        <input id="divPreEur" type="text" value="" style="width:96%;height:20px;padding:5px;border:1px solid #31A4D9;" />
                                    </td>
                                </tr>
                                <tr valign="top" style="padding: 5px;">
                                    <td colspan="2" style="padding:8px;">
                                        <input type="submit" class="button" value="Registrar gastos" onclick="guardaGasto();" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                    </td>
                    
                    <td style="width:1%">&nbsp;</td>
                    
                    <td width="50%" id="presupuesto"></td>
				</tr>				
			</table>
            
		</td>
			
        <td width="180" style="border-left: 1px solid #f1f1f1;">
            <table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left">
            
                        <table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:0px solid #259DD5;border-radius:0px;" >

                            <tr align="left" height="35" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                                <td colspan="4" style="padding:5px;width:100%;"><b>Opcions</b></td>
                            </tr>
                            
                            <tr><td>&nbsp;</td></tr>
                            
                            <tr>
                                <td align="left">
                                    <input type="submit" class="button" value="Nou Pressupost" onclick="habilitaNewPre();"/>
                                </td>
                            </tr>
                            
                            <tr><td>&nbsp;</td></tr>
                            
                            <tr id="nouPres" style="display:none;">
                                <td align="left">
                                    Any del pressupost: <input type="text" id="anyNewPres" maxlength="4" style="width: 35px;height:20px;border: 1px solid rgb(58, 166, 0);border-radius:3px;padding:2px;"/>
                                    <br>
                                    <br>
                                    <input type="submit" class="button" value="Crear Nou Pressupost" onclick="guardaNewPre();"/>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
				</tr>	
			</table>
        </td>
	</tr>

</table>