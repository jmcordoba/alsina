<?php
// incluimos clases
require_once 'clases/bbdd.php';
require_once 'clases/stats.php';

// creamos y validamos usuario
$stat = new estadistica();
$res  = $stat->insertaEstadistica();

// control de administrador
if ($_COOKIE[admin]!='si') {
    
    // redirect
	ob_clean();
    header("Location:menu.php");
    die();
}
?>

<!DOCTYPE html>

<html>
	<head>
		<title>Comunitat de propietaris Carrer Alsina i Maria Vidal de Vilasar de Mar</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<link rel="stylesheet"    href="css/vistasIndex.css" type="text/css" />
		<link rel="stylesheet"    href="css/menu_sup.css"    type="text/css" />
        <link rel="stylesheet"    href="css/comun.css"       type="text/css" />
		
        <script src="js/comun.js" language="javascript"></script>
        
        <link rel="shortcut icon" href="public/favico.ico" />
		
		<?php
		switch($_REQUEST['pagina']) {
            case 'vecinos': ?><link rel="stylesheet" href="css/08veci.css" type="text/css" /><?php break;
			default: break;
		}
		?>
	</head>
	
	<body cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;font-family:Verdana;" bgcolor="#888">
		
		<div style="position:absolute;top:0px;width:100%;height:60px;background-color:#31A4D9;border-bottom: 1px solid #259DD5;">
			<table style="height:60px;width:1000px;min-width:80%;margin:0 auto;">
				<tr>
					<td style="color:white;font-size:20px;">
						Comunitat de propietaris del Carrer Alsina i Maria Vidal
					</td>
					<td align="right" style="color:white;">
						Benvingut/da <b><?php echo $_COOKIE['nomb'];?></b>
                        &nbsp;|&nbsp;<a href="menu.php"       style="color:white;">web</a>
                        &nbsp;|&nbsp;<a href="desconecta.php" style="color:white;">desconecta</a>
					</td>
				</tr>
			</table>
		</div>
		
		<div style="position:absolute;top:70px;width:100%;height:85%;margin:0px;padding:0px;vertical-align:top;">
			
			<table border="0" cellspacing="0" cellpadding="0" style="height:100%;width:80%;min-width:1000px;margin:0 auto;background-color:white;border:1px solid #d1d1d1;box-shadow:0px 0px 20px 1px #CCC;border-radius:10px;" valign="top">
				<tr valign="top" height="60">
					<td align="left">
						<?php include 'menu_sup.php'; ?>
					</td>
				</tr>
				<tr valign="top">
					<td valign="top" width="100%">
						<?php
						switch($_REQUEST['pagina']) {
							
                            case 'vecinos':   include '08veci.php'; break;
							default:          include 'admin/01pres.php'; break;
						}
						?>
					</td>
				</tr>
			</table>
		</div>
		
	</body>
</html>