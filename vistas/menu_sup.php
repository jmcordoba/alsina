<?php

// inicializamos la clase de las pestañas NO seleccionadas
$classNews = 'border_no_selected_first';
$classLlum = 'border_no_selected';
$classNeEs = 'border_no_selected';
$classNePa = 'border_no_selected';
$classAsce = 'border_no_selected';
$classAssa = 'border_no_selected';
$classPres = 'border_no_selected';
$classCont = 'border_no_selected';
$classVeci = 'border_no_selected';

// inicializamos la clase de la pestaña SI seleccionada
switch($_REQUEST['pagina']) {
	
	case 'llum':      $classLlum = 'border_selected'; 		break;
	case 'netejae':   $classNeEs = 'border_selected'; 		break;
	case 'netejap':   $classNePa = 'border_selected'; 		break;
	case 'ascensor':  $classAsce = 'border_selected'; 		break;
	case 'seguro':    $classAssa = 'border_selected'; 		break;
	case 'presupost': $classPres = 'border_selected'; 		break;
	case 'contratos': $classCont = 'border_selected'; 		break;
    case 'vecinos':   $classVeci = 'border_selected'; 		break;
	default:          $classNews = 'border_selected_first'; break;
}
?>

<table cellpadding="0" cellspacing="0" valign="top" width="100%" height="60" style="margin:0px;padding:0px;">
	<tr valign="middle" width="100%" bgcolor="#f1f1f1">
		<td width="10%" align="center" class="<?php echo $classNews; ?>" onclick="location.href='menu.php?pagina=news'">
			<a href="menu.php?pagina=news" class="menu_sup_title_link">NOTÍCIES</a>
		</td>
        <td width="10%" align="center" class="<?php echo $classVeci; ?>" onclick="location.href='menu.php?pagina=vecinos'">
			<a href="menu.php?pagina=vecinos" class="menu_sup_title_link" >INFORMACIÓ<BR>DE VEÏNS</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classCont; ?>" onclick="location.href='menu.php?pagina=contratos'">
			<a href="menu.php?pagina=contratos" class="menu_sup_title_link" >CONTRACTES / DOCUMENTACIÓ</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classNeEs; ?>" onclick="location.href='menu.php?pagina=netejae'">
			<a href="menu.php?pagina=netejae" class="menu_sup_title_link" >NETEJA<BR>ESCALA</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classNePa; ?>" onclick="location.href='menu.php?pagina=netejap'">
			<a href="menu.php?pagina=netejap" class="menu_sup_title_link" >NETEJA<BR>PARKING</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classAsce; ?>" onclick="location.href='menu.php?pagina=ascensor'">
			<a href="menu.php?pagina=ascensor" class="menu_sup_title_link" >ASCENSOR</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classAssa; ?>" onclick="location.href='menu.php?pagina=seguro'">
			<a href="menu.php?pagina=seguro" class="menu_sup_title_link" >ASSAGURANÇA</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classLlum; ?>" onclick="location.href='menu.php?pagina=llum'">
			<a href="menu.php?pagina=llum" class="menu_sup_title_link" >FACTURES<BR>LLUM</a>
		</td>
		<td width="10%" align="center" class="<?php echo $classPres; ?>" onclick="location.href='menu.php?pagina=presupost'">
			<a href="menu.php?pagina=presupost" class="menu_sup_title_link" >PRESSUPOSTOS</a>
		</td>
		<td width="20''%" style="border-bottom:1px solid #d1d1d1;">&nbsp;</td>
	</tr>
</table>
