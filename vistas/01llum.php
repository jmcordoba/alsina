<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" height="100%" valign="top" style="padding:10px;marging:0px;">
			
				<tr width="100%" height="25" valign="top" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
					<td style="padding:5px;">
						<form action="menu.php">
							<table cellspacing="0" cellpadding="0" width="100%" style="padding:0px;margin:0px;">
								<tr align="right">
                                    <td width="130" align="left"><b>Busca la teva factura:</b></td>
									<td width="150" align="left">
										<select name="any" style="width:150px;height:25px;padding:2px;border:1px solid #888888;background:#ffffff;border-radius:3px;">
											<option />Any
											<option <?php if ($_REQUEST['any']=='2014')   echo 'selected'; ?> />2014
											<option <?php if ($_REQUEST['any']=='2013')   echo 'selected'; ?> />2013
										</select>
									</td>
									<td width="320" align="left">
										<select name="escala" style="width:150px;height:25px;padding:2px;border:1px solid #888888;background:#ffffff;border-radius:3px;">
											<option />Escala
											<option <?php if ($_REQUEST['escala']=='85')      echo 'selected'; ?> />85
											<option <?php if ($_REQUEST['escala']=='91')      echo 'selected'; ?> />91
											<option <?php if ($_REQUEST['escala']=='95')      echo 'selected'; ?> />95
											<option <?php if ($_REQUEST['escala']=='mvidal')  echo 'selected'; ?> />mvidal
											<option <?php if ($_REQUEST['escala']=='parking') echo 'selected'; ?> />parking
										</select>
									</td>
									<td width="100" align="right">
										<input type="submit" value="Busca" style="width:150px;height:25px;padding:2px;border:1px solid rgb(58, 166, 0);background-color:rgb(108, 211, 53);background-image: -moz-linear-gradient(center top , rgb(108, 211, 53), rgb(58, 166, 0));color:#ffffff;cursor:pointer;border-radius:3px;"/>
									</td>
								<tr>
							</table>
							
							<input type="hidden" name="pagina" value="llum" />
							<input type="hidden" name="accion" value="buscar" />
						</form>
					</td>
				</tr>
				
				<tr height="10"><td>&nbsp;</td></tr>
				
				<?php
				if( $_REQUEST['accion']=='buscar' and $_REQUEST['any']!='' and $_REQUEST['escala']!='' ) {
					
					//
					$factura = new facturaLlum();
					?>
					<tr height="10" valign="top">
						<td>
							<table cellspacing="0" cellpadding="0" width="100%" style="border:0px solid #259DD5;border-radius:0px;">
								
								<tr align="center" height="30" style="background-color:#31A4D9;color:#ffffff;">
									<td><b>Mes</b></td>
									<td><b>Consum (kWh)</b></td>
                                    <td><b>Factura (€)</b></td>
                                    <td><b>Document</b></td>
								</tr>
								
								<?php
								for($i=1;$i<13;$i++) {
									
									$fact = $factura->getFactura($_REQUEST['any'],$i,$_REQUEST['escala']);
									
									switch($i) {
										case 1:  $mes = 'Gener';    break;
										case 2:  $mes = 'Febrer';   break;
										case 3:  $mes = 'Març';     break;
										case 4:  $mes = 'Abril';    break;
										case 5:  $mes = 'Maig';     break;
										case 6:  $mes = 'Juny';     break;
										case 7:  $mes = 'Juliol';   break;
										case 8:  $mes = 'Agost';    break;
										case 9:  $mes = 'Setembre'; break;
										case 10: $mes = 'Octubre';  break;
										case 11: $mes = 'Novembre'; break;
										case 12: $mes = 'Desembre'; break;
									}
									
									if($i%2==0) $bgcolor = 'bgcolor="#f1f1f1"';
									else        $bgcolor = 'bgcolor="#ffffff"';
									
									foreach($fact as $row) {
										?>
										<tr id="fila" align="center" height="30" <?php echo $bgcolor; ?> >
											<td><?php echo $mes; ?></td>
                                            <?php
                                            $fact = str_replace("/alsina/", "", $row['enlace'])
                                            ?>
                                            <td><?php echo $row['consum']; ?></td>
                                            <td><?php echo $row['factura']; ?></td>
                                            <td>
                                                <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=<?php echo $fact; ?>" target="blank">
                                                    <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                                </a>
                                            </td>
										</tr>
										<?php
									}
								}
								?>
							</table>
						</td>
					</tr>
                    <tr height="10"><td>&nbsp;</td></tr>
					<?php
				}

				if($_COOKIE['admin']=='si') {
					?>
					<tr width="100%" valign="top">
						<td>
							<form name="formLlum" action="menu.php" method="post" enctype="multipart/form-data">
								<table id="novaFactura" cellspacing="0" cellpadding="0" width="100%" style="border:1px solid #259DD5;border-radius:3px;margin:0px;padding:10px;display:none;">
									<tr height="30">
										<td align="left" width="60">Any</td>
										<td align="left" width="150">
											<select name="anyNew" style="width:150px;height:30px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;">
												<option <?php if ($_REQUEST['anyNew']=='2014')   echo 'selected'; ?> />2014
												<option <?php if ($_REQUEST['anyNew']=='2013')   echo 'selected'; ?> />2013
											</select>
										</td>
										<td align="left" width="60">Mes</td>
										<td align="left" width="150">
											<select name="mesNew" style="width:150px;height:30px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;">
												<option value="1" />Gener
												<option value="2" />Febrer
												<option value="3" />Març
												<option value="4" />Abril
												<option value="5" />Maig
												<option value="6" />Juny
												<option value="7" />Juliol
												<option value="8" />Agost
												<option value="9" />Setembre
												<option value="10" />Octubre
												<option value="11" />Novembre
												<option value="12" />Desembre
											</select>
										</td>
										<td align="left" width="60">Escala</td>
										<td align="left" width="150">
											<select name="escalaNew" style="width:150px;height:30px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;">
												<option <?php if ($_REQUEST['escalaNew']=='85')      echo 'selected'; ?> />85
												<option <?php if ($_REQUEST['escalaNew']=='91')      echo 'selected'; ?> />91
												<option <?php if ($_REQUEST['escalaNew']=='95')      echo 'selected'; ?> />95
												<option <?php if ($_REQUEST['escalaNew']=='mvidal')  echo 'selected'; ?> />mvidal
												<option <?php if ($_REQUEST['escalaNew']=='parking') echo 'selected'; ?> />parking
											</select>
										</td>
									</tr>
									<tr height="30">
										<td align="left" width="60">Consum</td>
										<td><input type="text" name="consumNew"  id="consumNew" style="width:120px;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;" />&nbsp;kWh</td>
										<td align="left" width="60">Facturat</td>
										<td colspan="3"><input type="text" name="facturaNew" id="facturaNew" style="width:120px;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;" />&nbsp;€</td>
									</tr>
									<tr height="30">
										<td colspan="2" align="left" width="60">Factura en PDF</td>
										<td colspan="4"><input type="file" name="pdfNew"></td>
									</tr>
									<tr height="30" align="right">
										<td colspan="6">
											<input type="submit" value="Envia nova factura" class="gboton_green">
										</td>
									<tr>
								</table>
								<input type="hidden" name="pagina" value="llum" />
								<input type="hidden" name="accion" value="nuevaFactura" />
							</form>
						</td>
					</tr>
					<tr width="100%" valign="bottom">
						<td>
							<table cellspacing="0" cellpadding="0" width="100%" style="margin-top:20px;">
								<tr>						
									<td width="100%" align="right">
										<a href="#" onclick="nuevaFact();" class="gboton_green">Introdueix nova factura</a>
									</td>
								<tr>
							</table>
						</td>
					</tr>
					<?php
				}
				?>
			</table>
		</td>
		
		<td width="300" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
                
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2014
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/estudio_potencia_contratar_2014.pdf" target="blank">Estudi de potèncias optimes a contractar al 2014</a>
					</td>
				</tr>
                
                <tr><td>&nbsp;</td></tr>
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2013
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                        <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contador_individual.pdf"  target="blank">Manual del contador individual</a>
					</td>
				</tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contador_comunidad.pdf"   target="blank">Manual del contador comunitari</a>
					</td>
				</tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/estudio_consumo_2013.pdf" target="blank">Estudi de consum d'energia de la comunitat al 2013</a>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
		
</table>