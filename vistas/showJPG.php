<?php
/**
 * Muestra el JPEG que define la ruta que indica la variable $_GET['doc']
 * 
 * Este documento es incluido por otro código de tal manera que no es posible
 * acceder a los documentos guardados si el usuario no se ha registrado préviamente
 */

// obtenemos el documento a mostrar
$doc = $_GET['doc'];

// mostramos el documento
header('Content-Type: image/jpeg');
echo file_get_contents("$doc");
die();