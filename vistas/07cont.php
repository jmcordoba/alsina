<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left">
                        
						<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:0px solid #259DD5;border-radius:0px;" >

                            <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
								<td align="left" style="padding:5px;" colspan="4"><b>Descripció</b></td>
								<td align="left" style="padding:5px;"><b>Document</b></td>
							</tr>
							
                            <tr height="25" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Llei de Propietat Horitzontal</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/leyPropiedadHorizontal.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Divisió Horitzontal Comunitat</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/DivisionHorizontal.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Manteniment d'Ascensors</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/Multimax3G.jpg" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Comunicat de finalització de serveis a l'administrador Juan Espadas</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/comunicadoEnero2015.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
                            <tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Manual de l'amplificador de TDT</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/ManualTDT.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
                            <tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Extracte del compte bancari de la comunicat en el 2014</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/2015/extractoCuentas2014.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
                            <tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Comparativa d'empreses que realitzen feines de desatascament</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showXLS&doc=documentos/2015/ComparativaDesatascos.xls" target="_blank">
                                        <img src="public/XLS.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>







							
							<tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
								<td align="left" style="padding:5px;"><b>Contracte</b></td>
								<td align="left" style="padding:5px;"><b>Venciment</b></td>
								<td align="left" style="padding:5px;"><b>Cost</b></td>
								<td align="left" style="padding:5px;"><b>Possible estalvi</b></td>
								<td align="left" style="padding:5px;"><b>Document</b></td>
							</tr>
							
                            <tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Contracte de revisió d'extintors i boques d'incendi</td>
								<td align="left" style="padding:5px;">01/11/2014</td>
								<td align="left" style="padding:5px;">157,60 €/anuals</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/ContratoExtintores.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
                            <tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Contracte de llum del Parking</td>
								<td align="left" style="padding:5px;">01/08/2015</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/ContratoLuzParking01.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/ContratoLuzParking02.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/ContratoLuzParking03.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Assagurança comunitaria</td>
								<td align="left" style="padding:5px;">24/03/2015</td>
								<td align="left" style="padding:5px;">1450 €/anuals</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/seguro_comunidad.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Manteniment d'Ascensors</td>
								<td align="left" style="padding:5px;">18/04/2016</td>
								<td align="left" style="padding:5px;">1593,76 € per ascensor</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_mantenimiento_ascensor.JPG" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Neteja de escales</td>
								<td align="left" style="padding:5px;">Un mes d'antelació</td>
								<td align="left" style="padding:5px;">584,28 € per escala</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contratoBadaNet2014.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#ffffff;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Neteja de Parking</td>
								<td align="left" style="padding:5px;">Un mes d'antelació</td>
								<td align="left" style="padding:5px;">781,76 €</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">
                                    <a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contratoBadaNet2014.pdf" target="_blank">
                                        <img src="public/PDF.png" title="Descarrega document" width="25" height="25"/>
                                    </a>
                                </td>
							</tr>
							<tr height="38" style="background-color:#f1f1f1;color:#444444;cursor:pointer;">
								<td align="left" style="padding:5px;">Administrador</td>
								<td align="left" style="padding:5px;">Desembre</td>
								<td align="left" style="padding:5px;">3055,24 €</td>
								<td align="left" style="padding:5px;">&nbsp;</td>
								<td align="left" style="padding:5px;">No existeix, <br>la renovació es realitza <br>a la reunió ordinaria <br>del Desembre</td>
							</tr>
						</table>
					</td>
				</tr>				
			</table>
		</td>

	</tr>

</table>
