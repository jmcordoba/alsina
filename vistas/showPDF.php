<?php
/**
 * Muestra el PDF que define la ruta que indica la variable $_GET['doc']
 * 
 * Este documento es incluido por otro código de tal manera que no es posible
 * acceder a los documentos guardados si el usuario no se ha registrado préviamente
 */

// obtenemos el documento a mostrar
$doc = $_GET['doc'];

// mostramos el documento
header('Content-type: application/pdf');
readfile($doc);
die();
