<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Empresa</td>
					<td align="left" width="75%" style="padding: 10px;">BadaNet</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Freqüència de neteja</td>
					<td align="left" width="75%" style="padding: 10px;">1 vegada cada 15 dies</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Pagament mensual per escala (amb IVA)</td>
					<td align="left" width="75%" style="padding: 10px;">48,69 € (2014) 65,80 € (2013),  87,73 € (2012)</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Pagament anual per escala (amb IVA)</td>
					<td align="left" width="75%" style="padding: 10px;"><b>584,28 €</b> (2014) <b>, <b>789,60 €</b> (2013) <b>, 1.052,64 € (2012)</b> </td>
				</tr>
			</table>
		</td>
			
		<td width="300" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
				<tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2014 (BadaNet)
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contratoBadaNet2014.pdf" target="_blank">
							Contracte de neteja de les escales 2014
						</a>
					</td>
                </tr>
				
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2014
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_limpieza_2014.jpeg" target="_blank">
							Contracte de neteja de les escales 2014
						</a>
					</td>
                </tr>
                
                <tr><td>&nbsp;</td></tr>
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2013
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_limpieza_escalera.JPG" target="_blank">
						Contracte de neteja de les escales 2013</a>
					</td>
				<tr>
			</table>
		</td>
	</tr>

</table>