<?php

// cargamos Twig, ruta de plantillas y determinamos ruta de cache
require_once 'twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register(true);
$loader = new Twig_Loader_Filesystem('public/templates');
$twig   = new Twig_Environment(
				$loader/*, array(
				'cache' => 'public/cache',
				)*/
		  );

// inicializamos variables
$valores = array();

// cargamos variables
$valores["accion"] = $_REQUEST["accion"];

// carga template
echo $twig->render('index.twig', $valores);