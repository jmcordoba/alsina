<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" height="100%" valign="top" style="padding:10px;marging:0px;">
			
				<?php
                //
                $oVecinos = new vecino();
                $aVecinos = $oVecinos->getVecinos();
                ?>
                
                <tr valign="top">
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" style="border:0px solid #259DD5;border-radius:0px;">

                            <tr align="left" height="30" style="background-color:#31A4D9;color:#ffffff;">
                                <td style="padding:5px;"><b>Pis</b></td>
                                <td style="padding:5px;"><b>Nom</b></td>
                                <td style="padding:5px;"><b>Telèfon</b></td>
                                <td style="padding:5px;"><b>E-mail</b></td>
                                <td style="padding:5px;"><b>Càrrec</b></td>
                            </tr>

                            <?php
                            for($i=0;$i<count($aVecinos);$i++) {
                                
                                // definimos el color de fondo de la fila
                                if($i%2!=0) $bgcolor = 'bgcolor="#f1f1f1"';
                                else        $bgcolor = 'bgcolor="#ffffff"';

                                ?>
                                <tr id="fila" align="left" height="25" <?php echo $bgcolor; ?>>
                                    <td style="padding:5px;"><?php echo $aVecinos[$i]["piso"]; ?></td>
                                    <td style="padding:5px;"><?php echo $aVecinos[$i]["nomb"]; ?></td>
                                    <td style="padding:5px;"><?php echo $aVecinos[$i]["telf"]; ?></td>
                                    <td style="padding:5px;"><?php echo $aVecinos[$i]["mail"]; ?></td>

                                    <?php
                                    if($aVecinos[$i]["mail"]=='jmcordoba@gmail.com') {
                                        ?>
                                        <td style="padding:5px;">
                                        Vicepresident
                                        </td>
                                        <?php
                                    }                                    
                                    else if($aVecinos[$i]["mail"]=='vannes.bm@gmail.com') {
                                        ?>
                                        <td style="padding:5px;">
                                        President
                                        </td>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <td style="padding:5px;">&nbsp;</td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </td>
                </tr>
				
			</table>
		</td>
		
	</tr>
		
</table>
