<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
                <?php
                
                $comen = new comentario();
                $res   = $comen->getComentarios();
                
                for($i=0;$i<count($res);$i++) {
                    
                    $user  = $res[$i]["user"];
                    $texto = nl2br($res[$i]["comentario"]);
                    $fecha = $res[$i]["fecha"];
                    
                    $any = substr($fecha, 0, 4);
                    $mes = substr($fecha, 4, 2);
                    $day = substr($fecha, 6, 2);
                    $hor = substr($fecha, 8,  2);
                    $min = substr($fecha, 10, 2);
                    
                    $data = "$day/$mes/$any a les $hor:$min";
                    ?>
                    <tr>
                        <td align="left">

                            <table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:0px solid #259DD5;border-radius:0px;" >

                                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                                    <td style="padding:10px;">
                                        Escrit per <b><?php echo $user; ?></b> el <?php echo $data; ?>
                                    </td>
                                </tr>
                                <tr align="left" height="25" style="color:#000000;">
                                    <td style="padding:10px;text-align:justify;">
                                        <?php echo $texto; ?>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                
			</table>
		</td>
			
        <td width="300" style="border-left: 1px solid #f1f1f1;">
            
            <table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
                
                <form action="menu.php">
                    <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                        <td align="left" style="padding:10px;">
                            Deixa el teu comentari:
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td align="left">
                            <textarea name="comentario" style="width:95%;height:350px;border:1px solid #259DD5;border-radius:0px;padding:5px;font-size: 12px;font-family: Arial;"></textarea>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td align="left">
                            <input type="submit" value="Enviar *" style="width:100%;height:38px;padding:2px;border:1px solid #259DD5;background-color:#31A4D9;color:#ffffff;cursor:pointer;border-radius:3px;"/>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td align="left" style="text-align: justify;">
                            * Aquest missatge quedarà publicat automàticament i s'enviarà un correu electrónic a tots el veïns que tinguin l'adreça electrónica registrada en el web.
                        </td>
                    </tr>
                    <input type="hidden" name="pagina" value="news" />
					<input type="hidden" name="accion" value="nuevoComentario" />
                </form>
            </table>
        </td>
	</tr>

</table>