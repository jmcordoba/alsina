<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
				<tr style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
					
					<td colspan="3" valign="top" style="padding:1px;">
					
						<table cellspacing="0" cellpadding="0" width="100%" style="padding:5px;">
							
							<tr width="100%">
								<!--td align="left"><b>Sel·leciona les teves propietats:</b></td-->
								
								<td align="left">
									
									<?php
									$presu = new presupuesto();
									$listaPres = $presu->getListaPresupuestos();
									?>
									
									<b>Pressupost de l'any <?php echo $presu->any; ?></b>
									
									<select id="any" name="any" onchange="recargaPresupuesto();" style="height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:10px;">
										<?php
										for($i=0;$i<count($listaPres);$i++) {
											//
											if($any=='') {
												$presu->getPresupuesto($listaPres[0]['any']);
											} else {
												$presu->getPresupuesto($any);
											}
											?>
											<option value="<?php echo $listaPres[$i]['any']; ?>" <?php if($any==$listaPres[$i]['any']) { echo "selected"; } ?> >
											<?php echo $listaPres[$i]['any']; ?>
											</option>
											<?php
										}
										?>
									</select>
								</td>

								<td align="left">Pis</td>
								
								<td align="left">
									<select id="pisoSelect" name="pisoSelect" style="height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:10px;">
										<option value="No" <?php if($piso=="No") { echo "selected"; } ?> />Cap
										<option value="0"  <?php if($piso=="0")  { echo "selected"; } ?> />Escala 85 Pis Baixos 1ª
										<option value="1"  <?php if($piso=="1")  { echo "selected"; } ?> />Escala 85 Pis Baixos 2ª
										<option value="2"  <?php if($piso=="2")  { echo "selected"; } ?> />Escala 85 Pis Baixos 3ª
										<option value="3"  <?php if($piso=="3")  { echo "selected"; } ?> />Escala 85 Pis 1er 1ª
										<option value="4"  <?php if($piso=="4")  { echo "selected"; } ?> />Escala 85 Pis 1er 2ª
										<option value="5"  <?php if($piso=="5")  { echo "selected"; } ?> />Escala 85 Pis 1er 3ª
										<option value="6"  <?php if($piso=="6")  { echo "selected"; } ?> />Escala 85 Pis 2on 1ª
										<option value="7"  <?php if($piso=="7")  { echo "selected"; } ?> />Escala 85 Pis 2on 2ª
										<option value="8"  <?php if($piso=="8")  { echo "selected"; } ?> />Escala 85 Pis 2on 3ª
										<option value="9"  <?php if($piso=="9")  { echo "selected"; } ?> />Escala 91 Pis Baixos 1ª
										<option value="10" <?php if($piso=="10") { echo "selected"; } ?> />Escala 91 Pis Baixos 2ª
										<option value="11" <?php if($piso=="11") { echo "selected"; } ?> />Escala 91 Pis Baixos 3ª
										<option value="12" <?php if($piso=="12") { echo "selected"; } ?> />Escala 91 Pis 1er 1ª
										<option value="13" <?php if($piso=="13") { echo "selected"; } ?> />Escala 91 Pis 1er 2ª
										<option value="14" <?php if($piso=="14") { echo "selected"; } ?> />Escala 91 Pis 1er 3ª
										<option value="15" <?php if($piso=="15") { echo "selected"; } ?> />Escala 91 Pis 2on 1ª
										<option value="16" <?php if($piso=="16") { echo "selected"; } ?> />Escala 91 Pis 2on 2ª
										<option value="17" <?php if($piso=="17") { echo "selected"; } ?> />Escala 91 Pis 2on 3ª
										<option value="18" <?php if($piso=="18") { echo "selected"; } ?> />Escala 95 Pis Baixos 1ª
										<option value="19" <?php if($piso=="19") { echo "selected"; } ?> />Escala 95 Pis Baixos 2ª
										<option value="25" <?php if($piso=="20") { echo "selected"; } ?> />Escala 95 Pis 1er 1ª
										<option value="21" <?php if($piso=="21") { echo "selected"; } ?> />Escala 95 Pis 1er 2ª
										<option value="22" <?php if($piso=="22") { echo "selected"; } ?> />Escala 95 Pis 2on 1ª
										<option value="23" <?php if($piso=="23") { echo "selected"; } ?> />Escala 95 Pis 2on 2ª
										<option value="24" <?php if($piso=="24") { echo "selected"; } ?> />Maria Vidal Baixos 1ª
										<option value="25" <?php if($piso=="25") { echo "selected"; } ?> />Maria Vidal Baixos 2ª
									</select>
								</td>
							
								<td align="left">Parking</td>
								<td align="left">
									<select id="parkingSelect" name="parkingSelect" style="height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:10px;">
										<option value="No"/>Cap
										<?php
										for($i=1;$i<36;$i++) {
											?><option value="<?php echo $i+25; ?>" <?php if($parking==$i+25) { echo "selected"; } ?> />P<?php echo $i;
										}
										?>
									</select>
								</td>
								
								<td align="left">Traster</td>
								<td align="left">
									<select id="trasteroSelect" name="trasteroSelect" style="height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:10px;" >
										<option <?php if($traster=="No") { echo "selected"; } ?> />No
										<option <?php if($traster=="Si") { echo "selected"; } ?> />Si
									</select>
								</td>
							
								<td align="left">Parking motos</td>
								<td align="left">
									<select id="motoParkingSelect" name="motoParkingSelect" style="height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:10px;" >
										<option <?php if($traster=="No") { echo "selected"; } ?> />No
										<option <?php if($traster=="Si") { echo "selected"; } ?> />Si
									</select>
								</td>

                                <td align="right">
                                    <input type="submit" value="Calcula/Simula" onclick="calcula();simula();" style="width:100%;height:22px;padding:2px;border:1px solid #3AA600;background-color:#6CD335;color:#ffffff;cursor:pointer;border-radius:3px;">
                                </td>
							<tr>
						</table>

					</td>
				</tr>

				<tr><td>&nbsp;</td></tr>
				
				<tr width="100%">
					<td width="48%" valign="top">
					
						<table cellspacing="0" cellpadding="0" width="100%">
							
							<tr width="100%">
								<td align="left" colspan="2">
									<b>Aquest és el pressupost aprovat en la última reunió ordinaria</b>
								</td>
							<tr>
							
							<tr><td>&nbsp;</td></tr>
							<tr bgcolor="#f1f1f1">
								<td align="left"  style="width:70%;padding-left:5px;">Despesa comuns</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->comunes/100,2,',','')." €"; ?></td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 85</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->esc85/100,2,',','')." €"; ?></td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 91</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->esc91/100,2,',','')." €"; ?></td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 95</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->esc95/100,2,',','')." €"; ?></td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa de Maria Vidal</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->escMV/100,2,',','')." €"; ?></td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa de Parking</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->escPK/100,2,',','')." €"; ?></td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'Administrador</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->admin/100,2,',','')." €"; ?></td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">- per vivenda</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo "76,38 €" ?></td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">- per parking</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo "30,55 €"; ?></td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa Total</td>
								<td align="right" style="width:30%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;font-size:12px;color:black;"><?php echo number_format($presu->total/100,2,',','')." €"; ?></td>
							<tr>
							<tr><td>&nbsp;</td></tr>
							<tr height="25">
								<td align="left" style="width:70%">Cuota Anual</td>
								<td align="right" id="cuotaAnual" style="width:30%;height:15px;padding:3px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;color:red;font-size:12px;"></td>
							<tr>
							<tr height="25">
								<td align="left" style="width:70%">Cuota Trimestral</td>
								<td align="right" id="cuotaTrim" style="width:30%;height:15px;padding:3px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;color:red;font-size:12px;"></td>
							<tr>
							<tr><td>&nbsp;</td></tr>
							<tr height="25">
								<td align="right" style="width:100%" colspan="2">
                                    <input type="submit" value="Visualitza el simulador de cuotes >>" onclick="activaSimulador();" style="width:100%;height:22px;padding:2px;border:1px solid #259DD5;background-color:#31A4D9;color:#ffffff;cursor:pointer;border-radius:3px;">
								</td>
							<tr>
						</table>
					</td>
					
					<td width="4%">&nbsp;</td>
					
					<td width="48%" valign="top">
						<table cellspacing="0" cellpadding="0" width="100%" id="tablaSimulacion" style="display:none;">
							
							<?php
							$presu = new presupuesto();
							$presu->getPresupuesto($any);
							?>
							
							<tr width="100%">
								<td align="left" colspan="2">
									<b>Modifica les despeses i simula el que pagaries</b>
								</td>
							<tr>
							
							<tr><td>&nbsp;</td></tr>
							
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa comuns</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputComun" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->comunes/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;" />
								</td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 85</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputEsc85" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->esc85/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 91</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputEsc91" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->esc91/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'escala 95</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputEsc95" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->esc95/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa de Maria Vidal</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputEscMV" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->escMV/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;">Despesa de Parking</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputEscPK" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->escPK/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa d'Administrador</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputAdmin" readonly value="<?php echo number_format($presu->admin/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#f1f1f1;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;"> - per vivenda</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputAdminCasa" onkeyup="simulaGastoAdmin();" value="76,38" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr>
								<td align="left" style="width:70%;padding-left:5px;"> - per plaça de parking</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputAdminPark" onkeyup="simulaGastoAdmin();" value="30,55" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr bgcolor="#f1f1f1">
								<td align="left" style="width:70%;padding-left:5px;">Despesa Total</td>
								<td align="right" style="width:30%">
									<input type="text" id="inputTotal" onkeyup="calculaTotalSimulacion();" value="<?php echo number_format($presu->total/100,2,',',''); ?>" style="width:95%;height:22px;padding:2px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;text-align:right;"/>
								</td>
							<tr>
							<tr><td>&nbsp;</td></tr>
							<tr height="25">
								<td align="left" style="width:70%;padding-left:5px;">Cuota Anual</td>
								<td align="right" id="cuotaAnualSimulada" style="width:30%;height:15px;padding:3px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;color:red;font-size:12px;"></td>
							<tr>
							<tr height="25">
								<td align="left" style="width:70%;padding-left:5px;">Cuota Trimestral</td>
								<td align="right" id="cuotaTrimSimulada" style="width:30%;height:15px;padding:3px;border:1px solid #259DD5;background:#ffffff;border-radius:3px;color:red;font-size:12px;"></td>
							<tr>
							<tr><td>&nbsp;</td></tr>
							<tr height="25">
								<td colspan="2" align="right" style="padding-left:5px;">
                                    <input type="submit" value=">> Recupera el pressupost" onclick="recupera(<?php echo $presu->comunes; ?>,<?php echo $presu->esc85; ?>,<?php echo $presu->esc91; ?>,<?php echo $presu->esc95; ?>,<?php echo $presu->escMV; ?>,<?php echo $presu->escPK; ?>);" style="width:100%;height:22px;padding:2px;border:1px solid #259DD5;background-color:#31A4D9;color:#ffffff;cursor:pointer;border-radius:3px;">
								</td>
							<tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		
		<td width="200" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
				<tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2015
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/Presupuesto_2015.pdf" target="_blank">
						Pressupostos 2015
						</a>
					</td>
				</tr>
				
				<tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2014
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/Resumen_Ejercicio_2014.pdf" target="_blank">
						Resumen ejercicio 2014
						</a>
					</td>
                </tr>
				<tr><td>&nbsp;</td></tr>
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/acta_reunio_2014_04.jpeg" target="blank">
						Acta d'assistència de la reunió extraordinaria d'Abril de 2014
						</a>
					</td>
                </tr>
				<tr><td>&nbsp;</td></tr>
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/Presupuesto_2014.pdf" target="_blank">
						Pressupostos 2014
						</a>
					</td>
				</tr>
                
				<tr><td>&nbsp;</td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2013
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/Acta_RO_Decembre_2013.pdf" target="_blank">
						Acta Reunió Ordinaria Desembre 2013
						</a>
					</td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/Presupuesto_Extra_2013.pdf" target="_blank">
						Pressupost EXTRA 2013
						</a>
					</td>
                </tr>
                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/presupostos_2013.pdf" target="_blank">
						Pressupostos 2013
						</a>
					</td>
                </tr>
                
			</table>
		</td>
	</tr>
</table>

<?php
if($piso!="No" || $parking!="No" || $traster!="No" || $moto!="No") {
	?>
	<script>
	calcula();
	simula();
	</script>
	<?php
}
?>