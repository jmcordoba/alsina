<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Empresa mantenidora</td>
					<td align="left" width="75%" style="padding: 10px;">SOLER GRUP SL.</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Tipus d'instal·lació</td>
					<td align="left" width="75%" style="padding: 10px;">Electromecànic GEARLES</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Fabricant</td>
					<td align="left" width="75%" style="padding: 10px;">XINDA – Distribución España LIFT MACHINE</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Model</td>
					<td align="left" width="75%" style="padding: 10px;">DIANA-10-450-1S</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Potència</td>
					<td align="left" width="75%" style="padding: 10px;">4,3 CV</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">I nominal</td>
					<td align="left" width="75%" style="padding: 10px;">7,3 A</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">I d'arrancada</td>
					<td align="left" width="75%" style="padding: 10px;">10,5 A</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Consum</td>
					<td align="left" width="75%" style="padding: 10px;">4 kW / 10 A.</td>
				</tr>
				
			</table>
		</td>
		
		<td width="300" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/DIANA.pdf" target="_blank">
						Informació técnica de l'ascensor</a>
					</td>
				<tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_mantenimiento_ascensor.JPG" target="_blank">
						Contracte de l'ascensor</a>
					</td>
				<tr>
			</table>
		</td>
	</tr>
	
</table>