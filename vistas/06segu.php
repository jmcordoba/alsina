<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Assagurança de la comunitat</td>
					<td align="left" width="75%" style="padding: 10px;"><b>CATALANA OCCIDENTE</b></td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Telèfon d'informació i consultes</td>
					<td align="left" width="75%" style="padding: 10px;">902 206 208</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Venciment</td>
					<td align="left" width="75%" style="padding: 10px;"><b>ANUAL</b></td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Dia</td>
					<td align="left" width="75%" style="padding: 10px;">24/03/2016</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Import</td>
					<td align="left" width="75%" style="padding: 10px;">1.029,71 €</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Superfície vivendes</td>
					<td align="left" width="75%" style="padding: 10px;">2.046 m2</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Superfície parking</td>
					<td align="left" width="75%" style="padding: 10px;">1.436 m2</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Capital assagurat continent</td>
					<td align="left" width="75%" style="padding: 10px;">2.500.000 €</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Capital assagurat contingut</td>
					<td align="left" width="75%" style="padding: 10px;">3.000 €</td>
				</tr>
			</table>
		</td>
		
		<td width="300" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
                <tr><td>&nbsp;</td><tr>
                <tr>
					<td>2015</td>
				<tr>
                <tr><td>&nbsp;</td><tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/2015/SeguroCatalanaOccidente.pdf" target="_blank">
						Contracte de l'assagurança de la comunitat</a>
					</td>
				<tr>
                <tr><td>&nbsp;</td><tr>
				<tr>
					<td>2014</td>
				<tr>
                <tr><td>&nbsp;</td><tr>
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/seguro_comunidad.pdf" target="_blank">
						Contracte de l'assagurança de la comunitat</a>
					</td>
				<tr>
			</table>
		</td>
	</tr>
	
</table>
