<table cellspacing="0" cellpadding="0" width="100%" height="100%" style="padding:0px;marging:0px;">
	
	<tr width="100%" valign="top">
		<td>
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Empresa</td>
					<td align="left" width="75%" style="padding: 10px;">BadaNet</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Freqüència de neteja</td>
					<td align="left" width="75%" style="padding: 10px;">1 vegada al trimestre</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Pagament mensual (amb IVA)</td>
					<td align="left" width="75%" style="padding: 10px;">65,15 € (2014), 78,65 € (2013),  90,75 € (2012)</td>
				</tr>
				<tr>
					<td align="left" width="25%" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">Pagament anual (amb IVA)</td>
					<td align="left" width="75%" style="padding: 10px;"><b>781,76 €</b> (2014) <b>,<b>943,80 €</b> (2013) <b>, 1.089,00 €</b> (2012)</td>
				</tr>
			</table>
		</td>
		
		<td width="300" style="border-left: 1px solid #f1f1f1;">
			<table cellspacing="0" cellpadding="0" width="100%" style="padding:10px;marging:0px;">
				
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2015
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/2015/FacturaEmbus042015.pdf" target="_blank">
						Factura embús Abril de 2015
                        </a>
					</td>
                </tr>
                <tr><td>&nbsp;</td></tr>

				<tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2014 (BadaNet)
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showPDF&doc=documentos/contratoBadaNet2014.pdf" target="_blank">
						Contracte de neteja del parking</a>
					</td>
                </tr>
                <tr><td>&nbsp;</td></tr>

                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2013-2014
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
                <tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_limpieza_2014.jpeg" target="_blank">
						Contracte de neteja del parking</a>
					</td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr align="left" height="25" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                    <td align="left" style="padding:10px;">
                        2013
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                
				<tr>
					<td>
						<img src="public/PDF.png" title="Descarrega document" width="25" height="25" />
						<a href="http://www.navegalia.net/alsina/menu.php?accion=showJPG&doc=images/contrato_limpieza_parking.JPG" target="_blank">
						Contracte de neteja del parking</a>
					</td>
				<tr>
			</table>
		</td>
	</tr>
	
</table>
