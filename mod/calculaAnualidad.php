<?php

// control de credenciales
if( $_COOKIE['user']!='' and $_COOKIE['pass']!='' ) {
    
    // incluimos las clases
    include '../clases/bbdd.php';
    include '../clases/presupuesto.php';

    // creamos objeto
    $presupuesto = new presupuesto();

    // llamamos al metodo
    echo $presupuesto->getAnualidad($_REQUEST['idPiso'],$_REQUEST['idParking'],$_REQUEST['idTrastero'],$_REQUEST['idMotoP'],$_REQUEST['any']);
    
} else {
	
	// redirect
	ob_clean();
    header("Location:../index.php?accion=error");
    die();
}