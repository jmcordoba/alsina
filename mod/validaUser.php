<?php
/** 
 * Valida las credenciales introducidas por el usuario en la vista principal de
 * la aplicación, contrasta los datos con la bbdd.
 * Si son correctos: 
 *  - guarda info en COOKIES en el navegador
 *  - guarda estadistica de la visita
 *  - redirige a la página de menu
 * Si NO es correcta la autenticación:
 *  - redirige a la página inicial
 */

// inicializamos error > true
$error = true;

// validamos datos de entrada
if ( filter_var($_REQUEST["user"], FILTER_VALIDATE_EMAIL) && preg_match("/^[a-zA-Z0-9]+$/", $_REQUEST["pass"]) ) {

    // incluimos clases
    require_once 'clases/bbdd.php';
    require_once 'clases/user.php';

    // creamos y validamos usuario
    $usuario = new user();
    $res     = $usuario->getUser($_REQUEST["user"], $_REQUEST["pass"]);

    // si usuario validado
    if($res==true) {

        // OK
        $error = false;

        // registramos la visita del usuario solo cuando se valida
        $usuario->setVisita($_REQUEST["user"]);

        // redirigimos a menu
        header("Location:menu.php");
    }
} 

// si hemos detectado error
if ($error==true) {

    // redirect
    header("Location:index.php?accion=error");
}