<?php
// control de credenciales
if( $_COOKIE['user']!='' && $_COOKIE['pass']!='' && $_COOKIE['admin']=='si' ) {
    
    // incluimos las clases
    include '../../clases/bbdd.php';
    include '../../clases/presupuesto.php';

    // filtramos el dato que recojemos de la URL
    $res = filter_var($_GET['any'], FILTER_VALIDATE_INT);
    
    if ($res==true && strlen($_GET['any'])==4) {

        // creamos objeto
        $presupuesto = new presupuesto();

        // llamamos al metodo
        //echo $presupuesto->creaPresupuesto ($_GET['any']);
        
        ?>
        <table cellspacing="0" cellpadding="0" width="100%" height="100%" style="border:1px solid #259DD5;border-radius:0px;" >

            <tr align="left" height="35" style="background-color:#31A4D9;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>PRESSUPOST <?php echo $_GET['any']; ?></b></td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Comunes</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Escalera 85</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Escalera 91</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Escalera 95</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Maria Vidal</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Parking</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td valign="middle" colspan="2" style="padding:5px;width:100%;"><b>Administrador</b></td>
            </tr>
            <tr>
                <td align="left"  style="padding:5px;width:75%;background-color:#ffffff;">Seguro de la Comunidad</td>
                <td align="right" style="padding:5px;width:25%;background-color:#f1f1f1;color:#000000;">1500,55 €</td>
            </tr>

            <tr align="left" height="25" style="background-color:#888888;color:#ffffff;padding: 10px;">
                <td align="left"  valign="middle" style="padding:5px;width:75%;"><b>Total</b></td>
                <td align="right" valign="middle" style="padding:5px;width:25%;">20.000,00 €</td>
            </tr>

        </table>
        <?php
    } else {
        
        // devolvemos mensaje de error
        echo "L'any ha de tenir obligatoriament 4 valors numèrics.";
    }

} else {
	
	// redirect
	ob_clean();
    header("Location:../index.php?accion=error");
    die();
}
?>