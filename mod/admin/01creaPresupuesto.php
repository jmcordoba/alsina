<?php

// control de credenciales
if( $_COOKIE['user']!='' && $_COOKIE['pass']!='' && $_COOKIE['admin']=='si' ) {
    
    // incluimos las clases
    include '../../clases/bbdd.php';
    include '../../clases/presupuesto.php';

    // filtramos el dato que recojemos de la URL
    $res = filter_var($_GET['any'], FILTER_VALIDATE_INT);
    
    if ($res==true && strlen($_GET['any'])==4) {

        // creamos objeto
        $presupuesto = new presupuesto();

        // llamamos al metodo
        echo $presupuesto->creaPresupuesto ($_GET['any']);
        
    } else {
        
        // devolvemos mensaje de error
        echo "L'any ha de tenir obligatoriament 4 valors numèrics.";
    }

} else {
	
	// redirect
	ob_clean();
    header("Location:../index.php?accion=error");
    die();
}