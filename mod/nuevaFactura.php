<?php

// control de credenciales
if( $_COOKIE['user']!='' and $_COOKIE['pass']!='' ) {
    
    // creamos un objeto
    $factura = new facturaLlum();

    // calculamos la ruta del PDF
    $ruta = 'facturas/llum/'.$_REQUEST['escalaNew'].'/'.$_REQUEST['anyNew']."_".$_REQUEST['mesNew'].'.pdf';

    // guardamos el PDF

    // Capturamos los datos del fichero adjunto
    $nombre_archivo = $_FILES['pdfNew']['name']; 
    $tipo_archivo   = $_FILES['pdfNew']['type']; 
    $tamano_archivo = $_FILES['pdfNew']['size'];

    // si hemos introducido fichero
    if($nombre_archivo!="") {

        // lo intentamos guardar en la ruta especifica
        if (!move_uploaded_file($_FILES['pdfNew']['tmp_name'],'facturas/llum/'.$_REQUEST['escalaNew'].'/'.$_REQUEST['anyNew']."_".$_REQUEST['mesNew'].'.pdf')){
            // Si hay algún tipo de error >> alert
            ?>
			<script>
			alert('La factura en PDF no ha pogut ser guardada a la ruta especificada.');
			</script>
			<?php
        }
    }

    // si no existe la factura en la bbdd
    if(!$factura->getFacturaDuplicada($_REQUEST['anyNew'],$_REQUEST['mesNew'],$_REQUEST['escalaNew'])) {

        // guardamos la factura en la bbdd
        $fact = $factura->setFactura($_REQUEST['anyNew'],$_REQUEST['mesNew'],$_REQUEST['escalaNew'],$_REQUEST['consumNew'],$_REQUEST['facturaNew'],$ruta);
    } else {

        // misatge d'alerta
        ?>
		<script>
		alert('Ja existeix una factura enregistrada en aquest any, mes i escala.');
		</script>
		<?php
    }

    ?>

    <script>
    location.href="menu.php?any=<?php echo $_REQUEST['anyNew']; ?>&escala=<?php echo $_REQUEST['escalaNew']; ?>&pagina=llum&accion=buscar";
    </script>

    <?php
    
} else {
	
	// redirect
	ob_clean();
    header("Location:../index.php?accion=error");
    die();
}