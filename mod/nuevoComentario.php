<?php

// control de credenciales
if ($_COOKIE['user']!='' and $_COOKIE['pass']!='' ) {
    
    // obtenemos y tratamos el comentario
    $missatge = addslashes(utf8_decode($_REQUEST['comentario']));
    
    // obtenemos la lista de mails de vecinos
    $vecinos = new vecino();
    $veins   = $vecinos->getVecinosToNewsletter();
    
    // creamos un objeto y guardamos el comenatrio
    $comen = new comentario();
    $comen = $comen->setComentario($missatge);

    // enviamos mail a todos los vecinos registrados
    $news = new newsletter();
    $res  = $news->sendNewsletter($_COOKIE['nomb'], $missatge, $veins);

    // control de errores
    $text = '';
    
    // si no hemos posdido guardar el comentario
    if ($comen!=true) {
        $text .= 'No ha sido posible guardar el comentario.';
    }
    
    // si no hemos posdido enviar mails a los vecinos
    if ($res!=true) {
        $text .= '\n\rNo ha sido posible enviar el mail de aviso a los vecinos.';
    }

    // si hemos detectado algún error >> mostramos un alert
    if($text!='') {
        ?>
        <script>
        alert('<?php echo $text; ?>');
        </script>
        <?php
    }
    ?>
    <script>
    location.href="menu.php";
    </script>
    <?php
}
else
{
	// redirect
	ob_clean();
    header("Location:../index.php?accion=error");
    die();
}